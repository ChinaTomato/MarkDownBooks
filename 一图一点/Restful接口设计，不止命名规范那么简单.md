### Restful接口设计，不止命名规范那么简单

如何进行 RESTful 接口设计？主要考察两方面能力：

- 考察在之前工作中，是否具备**严谨、完整的编码风格与设计规约；**
- 考察是否具备一定的**代码设计能力与初级的架构思维。**

围绕这两方面，通过四个经典反向案例展开：

- 一类是 RESTful 书写规范本身的标准问题，通过 2 个反向案例，突破认知屏障，构建合理、优雅的 RESTful 接口；
- 另一类是代码设计与架构层面的知识，通过 2 个反向案例，展示你工作中习以为常的代码错误，及其更正方式。

“接口设计”是 IT 的通用技能，不仅限于开发，产品经理、项目经理、测试也都有学习的必要。

- 产品经理与项目经理作为项目的协调者，可以沟通需求时提前规划出 RESTful 接口标准，便于多团队间协同作业。
- 测试学习接口标准也很有必要，因为测试本身就是 QA（质量保证）的重要过程，在测试中解决、发现接口的潜在问题，对提升软件整体质量也有莫大帮助。

#### RESTful 接口的命名规范及语义格式

首先，重新认识 RESTful 接口，RESTful 接口的定义：

> RESTful 是一种网络应用程序的设计风格和开发方式，基于 HTTP 可以使用 XML 格式定义或 JSON 格式定义。RESTful 适用于移动互联网厂商作为业务接口的场景，实现第三方 OTT 调用移动网络资源的功能，动作类型为新增、变更、删除所调用资源。

在上面的描述中，RESTful 有三个要点：

- 基于 HTTP 协议 URL 对外暴露；
- 使用 XML 或 JSON 格式定义；
- 根据不同行为使用不同请求方式。

##### 1.基于 HTTP 协议 URL 对外暴露

在 RESTful 风格中，URL 被视为“资源”，要求使用名词进行说明，一般标准如下：

~~~tex
http(s)://域名:端口[/版本]/资源1[/子资源2/.../子资源n][/路径变量]
~~~

其中版本号是可选项，用于版本控制，通常使用 v1 / v1.1 这样的格式进行表达。如果版本号没有写，则默认使用最新版本获取资源。

在资源名中，可以采用多级结构进行细化，例如下面的 RESTful 接口就代表查询拉勾网博客模块下编号为 10 的 article 文章数据，调用的接口版本为 v1.1。在这段 RESTful 接口中，就包含了版本以及多级资源的描述。

~~~tex
GET http(s)://edu.lagou.com/v1.1/blog/article/10
~~~

对于要返回数据集合时，最后一级资源要采用复数。例如下面的 URL 说明查询分类编号为 10 的文章数据，调用的接口版本为 v1.1。

~~~tex
GET http(s)://edu.lagou.com/v1.1/blog/articles?categoryId=10
~~~

**【反面典型 1】**

反面典型：

~~~tex
GET http(s)://edu.lagou.com/article/select_by_id?id=10
~~~

**select_by_id 描述的是一个动作，而非代表资源的名词**。同时，如果稍微懂一点代码的黑客都可以猜到，这个 Controller 的方法名应该就是 selectById，这也无疑为系统留下了被攻击的风险。

##### 2.使用 XML 或 JSON 格式定义

RESTful 接口要求返回的是 JSON 或者 XML 数据，绝不可以包含任何与具体展现相关的内容。

**【反面典型 2】**

例如，要查询 10 号员工数据。

~~~tex
GET http(s)://edu.lagou.com/employee/10
{
    "code" : "0" ,
    "message" : "success" ,
    "data" : "<h1>员工姓名:张三</h1>..."
}
~~~

在上面的 JSON 响应中，data 选项内容是完全错误的，因为 h1 标签是 HTML 的内置标签，只有浏览器能够解析展现。如果假设客户端是安卓/IOS 的 APP，那岂不是无法使用？所以 RESTful 接口返回的数据一定要与展现无关。

如果返回数据改为下面的样式就是好的数据结构，最终数据如何展现将由客户端 HTML、APP 进行渲染。

~~~tex
GET http(s)://edu.lagou.com/employee/10
{
    "code" : "0" ,
    "message" : "success" ,
    "data" : {
        "name":"张三",
        "age" : 36
    }
}
~~~

##### 3.根据不同行为，使用不同请求方式

在 RESTful 语义中最常用的 GET、POST、PUT、DELETE 请求方式都有明确的用途，不能乱用。

- GET（SELECT）：从服务器取出资源。
- POST（CREATE）：在服务器新建一个资源。
- PUT（UPDATE）：在服务器更新资源。
- DELETE（DELETE）：从服务器删除资源。

同一个 URL 在不同请求方式下，在服务器端的操作也是完全不同的，举例说明。

下面的 URL 当使用 GET 方式，则代表向服务器查询编号为 10 的员工数据：

~~~tex
GET http(s)://edu.lagou.com/employee/10
~~~

但是请求换成 POST，同时在请求体附加员工 JSON 数据，就代表在服务器数据库中新增一个编号为 10 的员工：

~~~tex
POST http(s)://edu.lagou.com/employee/10
 {"id":10,"name":"张三","age" : 36}
~~~

类似的，PUT 代表更新员工数据：

~~~tex
PUT http(s)://edu.lagou.com/employee/10
 {"id":10,"name":"张三","age" : 38}
~~~

DELETE 代表删除员工数据：

~~~tex
DELETE http(s)://edu.lagou.com/employee/10
~~~

如果你的 RESTful 接口写得足够标准，其他工程师应该可以轻松地根据请求方式与 URL 的语义，猜出 RESTful 接口的作用，这也是 RESTful 风格的重要意义。

讲到这里，第一部分 RESTful 接口本身的书写要求就说完了。

但对于 RESTful 来说，只有好的命名是远不够的。在开发接口的过程中，还有需要注意的细节，接下来我就将带你进行梳理。

#### RESTful 接口的设计规则与注意事项

##### 1.接口要保证幂等性设计

在我们项目中，RESTful 接口设计的基本前提是保证幂等性。那什么是幂等性呢？说人话就是当多次重复请求时，接口能够保证与预期相符的结果。

我们举个例子来说明，例如我们设计了一个为员工涨薪的接口，本次请求发送后为 1 号员工涨薪 500 元。

~~~tex
PUT https://edu.lagou.com/employee/salary
{"id" : "1,"incr_salary":500}
~~~

**【反面典型 3】**

如果你是一个新人没有考虑幂等特性，可能会这么写，伪代码如下：

~~~tex
//查询1号员工数据
Employee employee = employeeService.selectById(1);
//更新工资
employee.setSalary(employee.getSalary() + incrSalary);
//执行更新语句
employeeService.update(employee)
~~~

对于这段代码，单独执行没有任何问题。

但我们要注意：在分布式环境下，为了保证消息的高可靠性，往往客户端会采用重试或者消息补偿的形式重复发送同一个请求。

那在这种情况下，这段代码就会出严重问题：每一个重复请求被发送到服务器都会让该员工工资加 500，最终该员工工资会大于实际应收工资。

那如何解决呢？在行业中有一种**乐观锁设计**，在员工表额外增加一个 version 的字段，代表当前数据的版本，任何对该数据修改操作都会让 version 字段值 +1，这个 version 的数值要求附加在请求中，如下所示：

~~~tex
PUT https://edu.lagou.com/employee/salary
{"id" : "1,"incr_salary":500 ,  "version":1}
~~~

执行更新操作前，1 号员工原始数据如下：

| id   | salary | version |
| ---- | ------ | ------- |
| 1    | 3000   | 1       |

而服务器端代码也作出如下调整，SQL 语句要增加版本号的比对。

~~~tex
//查询1号员工数据
Employee employee = employeeService.selectById(1);
//更新工资
employee.setSalary(employee.getSalary() + incrSalary);
//update执行成功，版本号+1
employee.setVersion(employee.getVersion() + 1));
//执行更新语句
employeeService.update(employee)
~~~

注意：此时 update 方法，除了 id 条件外，还要增加 version 的判断，如下所示。

~~~tex
update employee set salary = 3500,version=2 where id = 1 and version=1
~~~

当执行成功后，1 号员工原始数据将工资更新为 3500，版本为 2。

那这种设计如何保证幂等呢？假设客户端再次发送重复的请求：

~~~tex
PUT https://edu.lagou.com/employee/salary
{"id" : "1,"incr_salary":500 ,  "version":1}
~~~

后台按逻辑仍会执行下面的 update 语句：

~~~tex
update employee set salary = 3500,version=2 where id = 1 and version=1
~~~

此时，因为数据表中 version 字段已经更新为 2，where 筛选条件将无法得到任何数据。自然就不会产生实质的更新操作，我们幂等性的初衷就已经达到了。

以上是实现幂等性方案中 **MVCC 多版本并发控制方案**。除此以外，保证幂等性还可以采用**幂等表、分布式锁、状态机**等实现方式。

##### 2.标准化的响应结果集

RESTful 响应的 JSON 结构应当全局保持相同的结构与语义，这里我给出行业最常见的数据格式范例。

在**标准化的响应结构**中，要包含 code、message 两项，分别对应了服务器处理结果与返回的消息内容。除此以外，data 属性是可选项，包含从响应返回的额外数据，如查询结果、新增或更新后的数据。

在**语义层面**，也要遵循相同的规则。例如，当服务器处理成功，code 固定等于 0；如果遇到异常情况，公司内部也要遵循统一的 code 命名标准。例如：code 以 1xxx 开头代表参数异常，2xxx 开头代表数据库处理异常。

当然不同的公司有不同的命名规则，一定要提前定义好并要求开发团队严格按语义使用编码。

~~~tex
{
    code:"0" ,
    message : "success" ,
    data : {
        employee : {
            name : "张三",
            salary : 3500 , 
            version : 2
        }
    }
}
~~~

##### 3.接口设计采用无状态方案

**无状态方案，顾名思义 RESTful 接口每一个请求访问进来，应能得到相同的预期。**

经典设计：通过 Nginx 前置做负载均衡，将用户请求发送到后台 Tomcat 节点上。按早期设计，用户的数据会保存在 Tomcat 服务器的 Session 会话中。

> 假设浏览器将第 1 个请求发送到 1 号节点，在 1 号节点上保存了用户数据；之后，浏览器发送第 2 个请求，要读取该数据时，Nginx 按轮询规则将请求转发到 2 号节点上。因为 2 号节点 Session 中没有该用户数据，就会导致程序异常。

**【反向案例 4】**

分析这个现象的本质，是因为这些数据被分配到某一个 Tomcat 节点上，其他 Tomcat 节点没有该数据，这就导致 Tomcat 状态不一致，进而程序运行时就会产生预期外的问题。

![](D:\MarkDownBooks\一图一点\images\restful-1.png)

那如何解决呢，常用的**去状态化**的办法有以下两个。

- **客户端存储状态数据**

客户端存储，是指将用户数据保存在客户端的 Cookie 或者其他介质上，这些用户数据可以被附加在请求体或者请求头中，发送到服务器进行处理。因为服务器端不再存储任何数据，自然 RESTful 接口就不会存在状态问题。

但这种客户端存储也有新的问题：用户数据存储被包含在网络传输过程中，如果出现网络抓包等黑客行为可能会导致用户敏感信息泄露，造成额外的损失。因此，这些数据必须在客户端进行加密存储，在传输到服务器后再进行解密处理。

著名的 JWT（Json Web Token）就是采用这种方式实现用户认证的去状态化。

![](D:\MarkDownBooks\一图一点\images\restful-2.png)

- **后端统一存储状态数据**

  之所以后端出现状态问题，是因为数据都存储在某个 Tomcat 节点上，而这些 Tomcat 节点之间又没有进行有效同步，因此就会产生状态不一致的问题。如果在服务器后端将这些状态数据进行统一存储，所有 Tomcat 节点访问的都是相同的数据源，自然也不会产生状态问题。

  出于这种考虑，可以在 Tomcat 后端加入 Redis 进行统一保存。这样做的好处，是既可以保证用户敏感数据，又可以做到去状态化；但缺点也很明显，架构的复杂度增加，部署成本提高，需要单独采购高性能 Redis 服务器。

  ![](D:\MarkDownBooks\一图一点\images\restful-3.png)

以上便是 RESTful 接口去状态化的两种方案，大家可以根据项目的实际需要进行调整。

**小结**

- 第一部分，讲解了 RESTful 的命名规则与使用规范，让我们可以正确使用 RESTful 接口；
- 第二部分，介绍了 RESTful 的开发规则，包括幂等性、统一的响应结构、去状态化处理，可以有效指导你进行项目开发，让系统的性能与扩展性进一步提升。