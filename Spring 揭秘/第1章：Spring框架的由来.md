## 第一部分 掀起Spring的盖头来

### 第1章：Spring框架的由来

​	Spring是于2003年兴起的一个轻量级的Java开发框架，由Rod Johnson在其著作Expert One-On-One J2EE Development and Design中阐述的部分理念和原型衍生而来。它的最初目的主要是为了简化Java EE的企业级应用开发，相对于过去EJB时代重量级的企业应用开发而言，Spring框架的出现为曾经阴霾的天空带来了灿烂的阳光。

​	从广义上讲，不管Spring框架自发布到现在经过了多少次的版本更迭（从1.x到2.0再到2.5），其本质是始终不变的，都是为了提供各种服务，以帮助我们简化基于POJO的Java应用程序开发。Spring框架为POJO提供的各种服务共同组成了Spring的生命之树，如图1-1所示。

![](D:\MarkDownBooks\Spring揭秘\images\Spring框架总体结构.png)

​	无论从哪一个角度看，整个Spring家族都是富有活力、积极进取的，一旦有新的开发理念或者最佳实践涌现，我们通常会第一时间在Spring家族中发现它们的身影。随着整个Spring平台的发展，我们会看到Spring大观园将愈发地花团锦簇、欣欣向荣。看到这么一幅宏大而美丽的景象，你或许早就热血沸腾，想要马上投入Spring大观园之中。不过，正像Donald J. Trump在How To Get Rich一书中所说的那样：“Before the dream lifts you into the clouds, make sure look hard at the facts on the ground.”*（在被梦想搞得飘飘然之前，最好先让自己脚踏实地）*

## 第二部分 Spring的IOC容器

### 第2章：IOC的基本概念

​	全称为Inversion of Control，中文通常翻译为“控制反转”，它还有一个别名叫做依赖注入（Dependency
Injection）。好莱坞原则“Don’t call us, we will call you.”恰如其分地表达了“反转”的意味，是用来形容IoC最多的一句话。那么，为什么需要IoC？IoC的具体意义是什么？它到底有什么独到之处？

​	**三种注入方式的比较**

- 接口注入。从注入方式的使用上来说，接口注入是现在不甚提倡的一种方式，基本处于“退役状态”。因为它强制被注入对象实现不必要的接口，带有侵入性。而构造方法注入和setter方法注入则不需要如此。

- 构造方法注入。这种注入方式的优点就是，对象在构造完成之后，即已进入就绪状态，可以马上使用。缺点就是，当依赖对象比较多的时候，构造方法的参数列表会比较长。而通过反射构造对象的时候，对相同类型的参数的处理会比较困难，维护和使用上也比较麻烦。而且在Java中，构造方法无法被继承，无法设置默认值。对于非必须的依赖处理，可能需要引入多个构造方法，而参数数量的变动可能造成维护上的不便。

- setter方法注入。因为方法可以命名，所以setter方法注入在描述性上要比构造方法注入好一些。另外，setter方法可以被继承，允许设置默认值，而且有良好的IDE支持。缺点当然就是对象无法在构造完成后马上进入就绪状态。

  综上所述，构造方法注入和setter方法注入因为其侵入性较弱，且易于理解和使用，所以是现在使用最多的注入方式；而接口注入因为侵入性较强，近年来已经不流行了。

### 第3章：掌管大局的IOC Service Provider

### 第4章：Spring IOC容器之BeanFactory

### 第5章：Spring IOC容器ApplicationContext

### 第6章：Spring IOC容器之扩展篇

