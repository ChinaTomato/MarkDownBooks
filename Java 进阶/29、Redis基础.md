## 29、Redis基础

### Java缓存机制

Java中要用到缓存的地方很多，首当其冲的就是持久层缓存，针对持久层谈一下： 
要实现java缓存有很多种方式，最简单的无非就是static HashMap，这个显然是基于内存缓存，一个map就可以搞定引用对象的缓存，最简单也最不实用，首要的问题就是保存对象的有效性以及周期无法控制，这样很容易就导致内存急剧上升，周期无法控制可以采用SoftReference，WeakReference，PhantomReference这三种对象来执行（看了Ibatis的缓存机制才发现JDK居然还提供了PhantomReference这玩意儿，得恶补基础啊），这三种都是弱引用，区别在于强度不同，至于弱引用概念个人理解就是对象的生命周期与JVM挂钩，JVM内存不够了就回收，这样能很好的控制OutOfMemoryError 异常。 
常用的有Oscache,Ehcache,Jcache,Jbosscache等等很多

#### OsCache与EhCache区别

​	ehcache 主要是对数据库访问的缓存，相同的查询语句只需查询一次数据库，从而提高了查询的速度，使用spring的AOP可以很容易实现这一功能。

​	oscache 主要是对页面的缓存，可以整页或者指定网页某一部分缓存，同时指定他的过期时间，这样在此时间段里面访问的数据都是一样的。

### NoSQL介绍

​	NoSQL 是 Not Only SQL 的缩写，意即"不仅仅是SQL"的意思，泛指非关系型的数据库。强调Key-Value Stores和文档数据库的优点，而不是单纯的反对RDBMS。
​	NoSQL产品是传统关系型数据库的功能阉割版本，通过减少用不到或很少用的功能，来大幅度提高产品性能
​	NoSQL产品 Redis、mongodb Membase、HBase

#### Redis与Memcache的区别

Redis支持数据的持久化，可以将数据存放在硬盘上。
Memcache不支持数据的持久存储。
Redis数据类型丰富，支持set list等类型
Memcache支持简单数据类型，需要客户端自己处理复制对象

### Redis简介

#### Redis

Redis 是完全开源免费的，遵守BSD协议，是一个高性能的key-value数据库。
Redis 与其他 key - value 缓存产品有以下三个特点：
Redis支持数据的持久化，可以将内存中的数据保存在磁盘中，重启的时候可以再次加载进行使用。
Redis不仅仅支持简单的key-value类型的数据，同时还提供list，set，zset，hash等数据结构的存储。
Redis支持数据的备份，即master-slave模式的数据备份。

#### Redis应用场景

主要能够体现 解决数据库的访问压力。
例如：token生成、session共享、分布式锁、短信验证码时间有效期、自增id

#### Redis优势

性能极高 – Redis能读的速度是110000次/s,写的速度是81000次/s 。
丰富的数据类型 – Redis支持二进制案例的 Strings, Lists, Hashes, Sets 及 Ordered Sets 数据类型操作。
原子 – Redis的所有操作都是原子性的，同时Redis还支持对几个操作全并后的原子性执行。
丰富的特性 – Redis还支持 publish/subscribe, 通知, key 过期等等特性。

#### Redis与其他key-value存储的区别

​	Redis有着更为复杂的数据结构并且提供对他们的原子性操作，这是一个不同于其他数据库的进化路径。Redis的数据类型都是基于基本数据结构的同时对程序员透明，无需进行额外的抽象。
​	Redis运行在内存中但是可以持久化到磁盘，所以在对不同数据集进行高速读写时需要权衡内存，因为数据量不能大于硬件内存。在内存数据库方面的另一个优点是，相比在磁盘上相同的复杂的数据结构，在内存中操作起来非常简单，这样Redis可以做很多内部复杂性很强的事情。同时，在磁盘格式方面他们是紧凑的以追加的方式产生的，因为他们并不需要进行随机访问。

### Redis安装

#### windows安装redis

新建start.bat 批处理文件、内容: redis-server.exe  redis.windows.conf
双击start.bat启动
修改密码 # requirepass foobared  修改为requirepass 123456
==注意：修改密码的时requirepass前不允许空格，否则可能会报错==

#### linux安装redis

Redis的官方下载网址是：http://redis.io/download  (这里下载的是Linux版的Redis源码包）
Redis服务器端的默认端口是6379。
这里以虚拟机中的Linux系统如何安装Redis进行讲解。
 在windows系统中下载好Redis的源码包。
1. 通过WinSCP工具，将Redis的源码包由windows上传到Linux系统的这个目录/opt/redis (即根目录下的lamp文件夹）。
2. 解压缩。           
  tar -zxf redis-2.6.17.tar.gz
3. 切换到解压后的目录。
  cd redis-2.6.17            ( 一般来说，解压目录里的INSTALL文件或README文件里写有安装说明，可参考之）
4. 编译。
  make        
  （注意，编译需要C语言编译器gcc的支持，如果没有，需要先安装gcc。可以使用rpm -q gcc查看gcc是否安装）
  （利用yum在线安装gcc的命令    yum -y install gcc )
  （如果编译出错，请使用make clean清除临时文件。之后，找到出错的原因，解决问题后再来重新安装。 ）
5. 进入到src目录。       
  cd src
6. 执行安装。
  make install    
  到此就安装完成。但是，由于安装redis的时候，我们没有选择安装路径，故是默认位置安装。在此，我们可以将可执行文件和配置文件移动到习惯的目录。
  cd /usr/local
  mkdir -p /usr/local/redis/bin    
  mkdir -p /usr/local/redis/etc
  cd /lamp/redis-2.6.17
  cp ./redis.conf /usr/local/redis/etc
  cd src
  cp mkreleasehdr.sh redis-benchmark redis-check-aof redis-check-dump redis-cli redis-server redis-sentinel /usr/local/redis/bin
7. 开放linux 6379 端口
  1.编辑 /etc/sysconfig/iptables 文件：vi /etc/sysconfig/iptables
  加入内容并保存：-A RH-Firewall-1-INPUT -m state –state NEW -m tcp -p tcp –dport 6379 -j ACCEPT
  2.重启服务：/etc/init.d/iptables restart
  3.查看端口是否开放：/sbin/iptables -L -n
  比较重要的3个可执行文件：
  redis-server：Redis服务器程序
  redis-cli：Redis客户端程序，它是一个命令行操作工具。也可以使用telnet根据其纯文本协议操作。
  redis-benchmark：Redis性能测试工具，测试Redis在你的系统及配置下的读写性能。

##### Redis的启动命令

/usr/local/redis/bin/redis-server
或
cd /usr/local/redis/bin
./redis-server /usr/local/redis/etc/redis.conf    为redis-server指定配置文件

##### 修改redis.conf文件

daemonize yes --- 修改为yes  后台启动
requirepass 123456  ----注释取消掉设置账号密码
ps aux | grep '6379'  --- 查询端口
kill -15 9886 --- 杀死重置
kill -9 9886 --- 强制杀死
service iptables stop 停止防火墙

##### redis命令连接方式

redis使用账号密码连接

./redis-cli -h 127.0.0.1 -p 6379 -a "123456"

连接后，ping结果表示成功

##### 停止redis

redis-cli shutdown	或者	kill redis进程的pid

关闭防火墙：service iptables stop

### Redis客户端连接方式

redisclient-win32.x86.1.5

### Redis的基本数据类型

#### 字符串类型(String)

```shell
redis 127.0.0.1:6379> SET mykey "redis" 
OK 
redis 127.0.0.1:6379> GET mykey 
"redis"
```

在上面的例子中，SET和GET是redis中的命令，而mykey是键的名称。Redis字符串命令用于管理Redis中的字符串值。以下是使用Redis字符串命令的语法。

redis 127.0.0.1:6379> **COMMAND KEY_NAME**

下表列出了一些用于在Redis中管理字符串的基本命令：

| **编号** | **命令**                                                     | **描述说明**                               |
| -------- | ------------------------------------------------------------ | ------------------------------------------ |
| 1        | [SET   key value](http://www.yiibai.com/redis/strings_set.html) | 此命令设置指定键的值。                     |
| 2        | [GET key](http://www.yiibai.com/redis/strings_get.html)      | 获取指定键的值。                           |
| 3        | [GETRANGE key start end](http://www.yiibai.com/redis/strings_getrange.html) | 获取存储在键上的字符串的子字符串。         |
| 4        | [GETSET   key value](http://www.yiibai.com/redis/strings_getset.html) | 设置键的字符串值并返回其旧值。             |
| 5        | [GETBIT   key offset](http://www.yiibai.com/redis/strings_getbit.html) | 返回在键处存储的字符串值中偏移处的位值。   |
| 6        | [MGET   key1 [key2..\]](http://www.yiibai.com/redis/strings_mget.html) | 获取所有给定键的值                         |
| 7        | [SETBIT key offset value](http://www.yiibai.com/redis/strings_setbit.html) | 存储在键上的字符串值中设置或清除偏移处的位 |
| 8        | [SETEX key seconds value](http://www.yiibai.com/redis/strings_setex.html) | 使用键和到期时间来设置值                   |
| 9        | [SETNX   key value](http://www.yiibai.com/redis/strings_setnx.html) | 设置键的值，仅当键不存在时                 |
| 10       | [SETRANGE key offset value](http://www.yiibai.com/redis/strings_setrange.html) | 在指定偏移处开始的键处覆盖字符串的一部分   |
| 11       | [STRLEN key](http://www.yiibai.com/redis/strings_strlen.html) | 获取存储在键中的值的长度                   |
| 12       | MSET key value[ [key value …\]](http://www.yiibai.com/redis/strings_mset.html) | 为多个键分别设置它们的值                   |
| 13       | MSETNX key value[ [key value …\]](http://www.yiibai.com/redis/strings_msetnx.html) | 为多个键分别设置它们的值，仅当键不存在时   |
| 14       | [PSETEX key milliseconds value](http://www.yiibai.com/redis/strings_psetex.html) | 设置键的值和到期时间(以毫秒为单位)         |
| 15       | [INCR key](http://www.yiibai.com/redis/strings_incr.html)    | 将键的整数值增加`1`                        |
| 16       | [INCRBY key increment](http://www.yiibai.com/redis/strings_incrby.html) | 将键的整数值按给定的数值增加               |
| 17       | [INCRBYFLOAT key increment](http://www.yiibai.com/redis/strings_incrbyfloat.html) | 将键的浮点值按给定的数值增加               |
| 18       | [DECR key](http://www.yiibai.com/redis/strings_decr.html)    | 将键的整数值减`1`                          |
| 19       | [DECRBY key decrement](http://www.yiibai.com/redis/strings_decrby.html) | 按给定数值减少键的整数值                   |
| 20       | [APPEND   key value](http://www.yiibai.com/redis/strings_append.html) | 将指定值附加到键                           |

#### 列表类型(list)

Redis列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）
一个列表最多可以包含 2的32次方 - 1 个元素 (4294967295, 每个列表超过40亿个元素)。

```shell
redis 127.0.0.1:6379> LPUSH runoobkey redis
(integer) 1
redis 127.0.0.1:6379> LPUSH runoobkey mongodb
(integer) 2
redis 127.0.0.1:6379> LPUSH runoobkey mysql
(integer) 3
redis 127.0.0.1:6379> LRANGE runoobkey 0 10

1) "mysql"
2) "mongodb"
3) "redis"
```

Redis 列表命令
下表列出了列表相关的基本命令：

| **序号** | **命令及描述**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | BLPOP key1[  [key2 \] timeout](http://www.runoob.com/redis/lists-blpop.html)     移出并获取列表的第一个元素， 如果列表没有元素会阻塞列表直到等待超时或发现可弹出元素为止。 |
| 2        | BRPOP key1[  [key2 \] timeout](http://www.runoob.com/redis/lists-brpop.html)     移出并获取列表的最后一个元素， 如果列表没有元素会阻塞列表直到等待超时或发现可弹出元素为止。 |
| 3        | [BRPOPLPUSH source destination timeout](http://www.runoob.com/redis/lists-brpoplpush.html)     从列表中弹出一个值，将弹出的元素插入到另外一个列表中并返回它； 如果列表没有元素会阻塞列表直到等待超时或发现可弹出元素为止。 |
| 4        | [LINDEX key index](http://www.runoob.com/redis/lists-lindex.html)     通过索引获取列表中的元素 |
| 5        | [LINSERT key BEFORE\|AFTER pivot value](http://www.runoob.com/redis/lists-linsert.html)     在列表的元素前或者后插入元素 |
| 6        | [LLEN key](http://www.runoob.com/redis/lists-llen.html)     获取列表长度 |
| 7        | [LPOP key](http://www.runoob.com/redis/lists-lpop.html)     移出并获取列表的第一个元素 |
| 8        | LPUSH key value1[  [value2\]](http://www.runoob.com/redis/lists-lpush.html)     将一个或多个值插入到列表头部 |
| 9        | [LPUSHX key value](http://www.runoob.com/redis/lists-lpushx.html)     将一个值插入到已存在的列表头部 |
| 10       | [LRANGE key start stop](http://www.runoob.com/redis/lists-lrange.html)     获取列表指定范围内的元素 |
| 11       | [LREM key count value](http://www.runoob.com/redis/lists-lrem.html)     移除列表元素 |
| 12       | [LSET key index value](http://www.runoob.com/redis/lists-lset.html)     通过索引设置列表元素的值 |
| 13       | [LTRIM key start stop](http://www.runoob.com/redis/lists-ltrim.html)     对一个列表进行修剪(trim)，就是说，让列表只保留指定区间内的元素，不在指定区间之内的元素都将被删除。 |
| 14       | [RPOP key](http://www.runoob.com/redis/lists-rpop.html)     移除并获取列表最后一个元素 |
| 15       | [RPOPLPUSH source destination](http://www.runoob.com/redis/lists-rpoplpush.html)     移除列表的最后一个元素，并将该元素添加到另一个列表并返回 |
| 16       | RPUSH key value1[  [value2\]](http://www.runoob.com/redis/lists-rpush.html)     在列表中添加一个或多个值 |
| 17       | [RPUSHX key value](http://www.runoob.com/redis/lists-rpushx.html)     为已存在的列表添加值 |

#### Redis集合(Set)

Redis的Set是string类型的无序集合。集合成员是唯一的，这就意味着集合中不能出现重复的数据。
Redis 中 集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是O(1)。
集合中最大的成员数为 2的32次方 - 1 (4294967295, 每个集合可存储40多亿个成员)。

示例：

```shell
redis 127.0.0.1:6379> SADD runoobkey redis
(integer) 1
redis 127.0.0.1:6379> SADD runoobkey mongodb
(integer) 1
redis 127.0.0.1:6379> SADD runoobkey mysql
(integer) 1
redis 127.0.0.1:6379> SADD runoobkey mysql
(integer) 0
redis 127.0.0.1:6379> SMEMBERS runoobkey

1) "mysql"
2) "mongodb"
3) "redis"
```

在以上实例中我们通过 **SADD** 命令向名为 runoobkey 的集合插入的三个元素。

Redis 集合命令
下表列出了 Redis 集合基本命令：

| **序号** | **命令及描述**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | [SADD key member1 [member2\]](http://www.runoob.com/redis/sets-sadd.html)     向集合添加一个或多个成员 |
| 2        | [SCARD key](http://www.runoob.com/redis/sets-scard.html)     获取集合的成员数 |
| 3        | [SDIFF key1 [key2\]](http://www.runoob.com/redis/sets-sdiff.html)     返回给定所有集合的差集 |
| 4        | [SDIFFSTORE destination key1 [key2\]](http://www.runoob.com/redis/sets-sdiffstore.html)     返回给定所有集合的差集并存储在 destination 中 |
| 5        | [SINTER key1 [key2\]](http://www.runoob.com/redis/sets-sinter.html)     返回给定所有集合的交集 |
| 6        | [SINTERSTORE destination key1 [key2\]](http://www.runoob.com/redis/sets-sinterstore.html)     返回给定所有集合的交集并存储在 destination 中 |
| 7        | [SISMEMBER key member](http://www.runoob.com/redis/sets-sismember.html)     判断 member 元素是否是集合 key 的成员 |
| 8        | [SMEMBERS key](http://www.runoob.com/redis/sets-smembers.html)     返回集合中的所有成员 |
| 9        | [SMOVE source destination member](http://www.runoob.com/redis/sets-smove.html)     将 member 元素从 source 集合移动到 destination 集合 |
| 10       | [SPOP key](http://www.runoob.com/redis/sets-spop.html)     移除并返回集合中的一个随机元素 |
| 11       | [SRANDMEMBER key [count\]](http://www.runoob.com/redis/sets-srandmember.html)     返回集合中一个或多个随机数 |
| 12       | [SREM key member1 [member2\]](http://www.runoob.com/redis/sets-srem.html)     移除集合中一个或多个成员 |
| 13       | [SUNION key1 [key2\]](http://www.runoob.com/redis/sets-sunion.html)     返回所有给定集合的并集 |
| 14       | [SUNIONSTORE destination key1 [key2\]](http://www.runoob.com/redis/sets-sunionstore.html)     所有给定集合的并集存储在 destination 集合中 |
| 15       | [SSCAN key cursor [MATCH pattern\] [COUNT count]](http://www.runoob.com/redis/sets-sscan.html)     迭代集合中的元素 |

#### Redis有序集合(sorted set)

Redis 有序集合和集合一样也是string类型元素的集合,且不允许重复的成员。
不同的是每个元素都会关联一个double类型的分数。redis正是通过分数来为集合中的成员进行从小到大的排序。
有序集合的成员是唯一的,但分数(score)却可以重复。
集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是O(1)。 集合中最大的成员数为 2的32次方 - 1 (4294967295, 每个集合可存储40多亿个成员)。

示例：

```shell
redis 127.0.0.1:6379> ZADD runoobkey 1 redis
(integer) 1
redis 127.0.0.1:6379> ZADD runoobkey 2 mongodb
(integer) 1
redis 127.0.0.1:6379> ZADD runoobkey 3 mysql
(integer) 1
redis 127.0.0.1:6379> ZADD runoobkey 3 mysql
(integer) 0
redis 127.0.0.1:6379> ZADD runoobkey 4 mysql
(integer) 0
redis 127.0.0.1:6379> ZRANGE runoobkey 0 10 WITHSCORES

1) "redis"
2) "1"
3) "mongodb"
4) "2"
5) "mysql"
6) "4"
```

在以上实例中我们通过命令 **ZADD** 向 redis 的有序集合中添加了三个值并关联上分数。

Redis 有序集合命令
下表列出了 redis 有序集合的基本命令：

| **序号** | **命令及描述**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | [ZADD key score1 member1 [score2 member2\]](http://www.runoob.com/redis/sorted-sets-zadd.html)     向有序集合添加一个或多个成员，或者更新已存在成员的分数 |
| 2        | [ZCARD key](http://www.runoob.com/redis/sorted-sets-zcard.html)     获取有序集合的成员数 |
| 3        | [ZCOUNT key min max](http://www.runoob.com/redis/sorted-sets-zcount.html)     计算在有序集合中指定区间分数的成员数 |
| 4        | [ZINCRBY key increment member](http://www.runoob.com/redis/sorted-sets-zincrby.html)     有序集合中对指定成员的分数加上增量 increment |
| 5        | [ZINTERSTORE destination numkeys key [key ...\]](http://www.runoob.com/redis/sorted-sets-zinterstore.html)     计算给定的一个或多个有序集的交集并将结果集存储在新的有序集合 key 中 |
| 6        | [ZLEXCOUNT key min max](http://www.runoob.com/redis/sorted-sets-zlexcount.html)     在有序集合中计算指定字典区间内成员数量 |
| 7        | [ZRANGE key start stop [WITHSCORES\]](http://www.runoob.com/redis/sorted-sets-zrange.html)     通过索引区间返回有序集合成指定区间内的成员 |
| 8        | [ZRANGEBYLEX key min max [LIMIT offset count\]](http://www.runoob.com/redis/sorted-sets-zrangebylex.html)     通过字典区间返回有序集合的成员 |
| 9        | [ZRANGEBYSCORE key min max [WITHSCORES\] [LIMIT]](http://www.runoob.com/redis/sorted-sets-zrangebyscore.html)     通过分数返回有序集合指定区间内的成员 |
| 10       | [ZRANK key member](http://www.runoob.com/redis/sorted-sets-zrank.html)     返回有序集合中指定成员的索引 |
| 11       | [ZREM key member [member ...\]](http://www.runoob.com/redis/sorted-sets-zrem.html)     移除有序集合中的一个或多个成员 |
| 12       | [ZREMRANGEBYLEX key min max](http://www.runoob.com/redis/sorted-sets-zremrangebylex.html)     移除有序集合中给定的字典区间的所有成员 |
| 13       | [ZREMRANGEBYRANK key start stop](http://www.runoob.com/redis/sorted-sets-zremrangebyrank.html)     移除有序集合中给定的排名区间的所有成员 |
| 14       | [ZREMRANGEBYSCORE key min max](http://www.runoob.com/redis/sorted-sets-zremrangebyscore.html)     移除有序集合中给定的分数区间的所有成员 |
| 15       | [ZREVRANGE key start stop [WITHSCORES\]](http://www.runoob.com/redis/sorted-sets-zrevrange.html)     返回有序集中指定区间内的成员，通过索引，分数从高到底 |
| 16       | [ZREVRANGEBYSCORE key max min [WITHSCORES\]](http://www.runoob.com/redis/sorted-sets-zrevrangebyscore.html)     返回有序集中指定分数区间内的成员，分数从高到低排序 |
| 17       | [ZREVRANK key member](http://www.runoob.com/redis/sorted-sets-zrevrank.html)     返回有序集合中指定成员的排名，有序集成员按分数值递减(从大到小)排序 |
| 18       | [ZSCORE key member](http://www.runoob.com/redis/sorted-sets-zscore.html)     返回有序集中，成员的分数值 |
| 19       | [ZUNIONSTORE destination numkeys key [key ...\]](http://www.runoob.com/redis/sorted-sets-zunionstore.html)     计算给定的一个或多个有序集的并集，并存储在新的 key 中 |
| 20       | [ZSCAN key cursor [MATCH pattern\] [COUNT count]](http://www.runoob.com/redis/sorted-sets-zscan.html)     迭代有序集合中的元素（包括元素成员和元素分值） |

#### Redis哈希(Hash)

Redis hash 是一个string类型的field和value的映射表，hash特别适合用于存储对象。
Redis 中每个 hash 可以存储 2的32次方 - 1 键值对（40多亿）。

示例：

```shell
127.0.0.1:6379>  HMSET runoobkey name "redis tutorial" 
127.0.0.1:6379>  HGETALL runoobkey
1) "name"
2) "redis tutorial"
3) "description"
4) "redis basic commands for caching"
5) "likes"
6) "20"
7) "visitors"
8) "23000"
```

**hset  key  mapHey MapValue**
在以上实例中，我们设置了 redis 的一些描述信息(name, description, likes, visitors) 到哈希表的 **runoobkey** 中。

Redis hash 命令
下表列出了 redis hash 基本的相关命令：

| **序号** | **命令及描述**                                               |
| -------- | ------------------------------------------------------------ |
| 1        | [HDEL key field2 [field2\]](http://www.runoob.com/redis/hashes-hdel.html)     删除一个或多个哈希表字段 |
| 2        | [HEXISTS key field](http://www.runoob.com/redis/hashes-hexists.html)     查看哈希表 key 中，指定的字段是否存在。 |
| 3        | [HGET key field](http://www.runoob.com/redis/hashes-hget.html)     获取存储在哈希表中指定字段的值。 |
| 4        | [HGETALL key](http://www.runoob.com/redis/hashes-hgetall.html)     获取在哈希表中指定 key 的所有字段和值 |
| 5        | [HINCRBY key field increment](http://www.runoob.com/redis/hashes-hincrby.html)     为哈希表 key 中的指定字段的整数值加上增量 increment 。 |
| 6        | [HINCRBYFLOAT key field increment](http://www.runoob.com/redis/hashes-hincrbyfloat.html)     为哈希表 key 中的指定字段的浮点数值加上增量 increment 。 |
| 7        | [HKEYS key](http://www.runoob.com/redis/hashes-hkeys.html)     获取所有哈希表中的字段 |
| 8        | [HLEN key](http://www.runoob.com/redis/hashes-hlen.html)     获取哈希表中字段的数量 |
| 9        | [HMGET key field1 [field2\]](http://www.runoob.com/redis/hashes-hmget.html)     获取所有给定字段的值 |
| 10       | [HMSET key field1 value1 [field2 value2 \]](http://www.runoob.com/redis/hashes-hmset.html)     同时将多个 field-value (域-值)对设置到哈希表 key 中。 |
| 11       | [HSET key field value](http://www.runoob.com/redis/hashes-hset.html)     将哈希表 key 中的字段 field 的值设为 value 。 |
| 12       | [HSETNX key field value](http://www.runoob.com/redis/hashes-hsetnx.html)     只有在字段 field 不存在时，设置哈希表字段的值。 |
| 13       | [HVALS key](http://www.runoob.com/redis/hashes-hvals.html)     获取哈希表中所有值 |
| 14       | HSCAN key cursor [MATCH pattern][COUNT count]     迭代哈希表中的键值对。 |

### Java操作Redis

#### Redis Jedis

```xml
  <!-- https://mvnrepository.com/artifact/redis.clients/jedis -->
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>2.9.0</version>
</dependency>
```

```java
public class TestRedis {
    private Jedis jedis; 
    
    @Before
    public void setup() {
        //连接redis服务器，192.168.0.100:6379
        jedis = new Jedis("192.168.0.100", 6379);
        //权限认证
        jedis.auth("admin");  
    }
    
    /**
     * redis存储字符串
     */
    @Test
    public void testString() {
        //-----添加数据----------  
        jedis.set("name","xinxin");//向key-->name中放入了value-->xinxin  
        System.out.println(jedis.get("name"));//执行结果：xinxin  
        
        jedis.append("name", " is my lover"); //拼接
        System.out.println(jedis.get("name")); 
        
        jedis.del("name");  //删除某个键
        System.out.println(jedis.get("name"));
        //设置多个键值对
        jedis.mset("name","liuling","age","23","qq","476777XXX");
        jedis.incr("age"); //进行加1操作
        System.out.println(jedis.get("name") + "-" + jedis.get("age") + "-" + jedis.get("qq"));
        
    }
    
    /**
     * redis操作Map
     */
    @Test
    public void testMap() {
        //-----添加数据----------  
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "xinxin");
        map.put("age", "22");
        map.put("qq", "123456");
        jedis.hmset("user",map);
        //取出user中的name，执行结果:[minxr]-->注意结果是一个泛型的List  
        //第一个参数是存入redis中map对象的key，后面跟的是放入map中的对象的key，后面的key可以跟多个，是可变参数  
        List<String> rsmap = jedis.hmget("user", "name", "age", "qq");
        System.out.println(rsmap);  
  
        //删除map中的某个键值  
        jedis.hdel("user","age");
        System.out.println(jedis.hmget("user", "age")); //因为删除了，所以返回的是null  
        System.out.println(jedis.hlen("user")); //返回key为user的键中存放的值的个数2 
        System.out.println(jedis.exists("user"));//是否存在key为user的记录 返回true  
        System.out.println(jedis.hkeys("user"));//返回map对象中的所有key  
        System.out.println(jedis.hvals("user"));//返回map对象中的所有value 
  
        Iterator<String> iter=jedis.hkeys("user").iterator();  
        while (iter.hasNext()){  
            String key = iter.next();  
            System.out.println(key+":"+jedis.hmget("user",key));  
        }  
    }
    
    /** 
     * jedis操作List 
     */  
    @Test  
    public void testList(){  
        //开始前，先移除所有的内容  
        jedis.del("java framework");  
        System.out.println(jedis.lrange("java framework",0,-1));  
        //先向key java framework中存放三条数据  
        jedis.lpush("java framework","spring");  
        jedis.lpush("java framework","struts");  
        jedis.lpush("java framework","hibernate");  
        //再取出所有数据jedis.lrange是按范围取出，  
        // 第一个是key，第二个是起始位置，第三个是结束位置，jedis.llen获取长度 -1表示取得所有  
        System.out.println(jedis.lrange("java framework",0,-1));  
        
        jedis.del("java framework");
        jedis.rpush("java framework","spring");  
        jedis.rpush("java framework","struts");  
        jedis.rpush("java framework","hibernate"); 
        System.out.println(jedis.lrange("java framework",0,-1));
    }  
    
    /** 
     * jedis操作Set 
     */  
    @Test  
    public void testSet(){  
        //添加  
        jedis.sadd("user","liuling");  
        jedis.sadd("user","xinxin");  
        jedis.sadd("user","ling");  
        jedis.sadd("user","zhangxinxin");
        jedis.sadd("user","who");  
        //移除noname  
        jedis.srem("user","who");  
        System.out.println(jedis.smembers("user"));//获取所有加入的value  
        System.out.println(jedis.sismember("user", "who"));//判断 who 是否是user集合的元素  
        System.out.println(jedis.srandmember("user"));  
        System.out.println(jedis.scard("user"));//返回集合的元素个数  
    }  
  
    @Test  
    public void test() throws InterruptedException {  
        //jedis 排序  
        //注意，此处的rpush和lpush是List的操作。是一个双向链表（但从表现来看的）  
        jedis.del("a");//先清除数据，再加入数据进行测试  
        jedis.rpush("a", "1");  
        jedis.lpush("a","6");  
        jedis.lpush("a","3");  
        jedis.lpush("a","9");  
        System.out.println(jedis.lrange("a",0,-1));// [9, 3, 6, 1]  
        System.out.println(jedis.sort("a")); //[1, 3, 6, 9]  //输入排序后结果  
        System.out.println(jedis.lrange("a",0,-1));  
    }  
    
    @Test
    public void testRedisPool() {
        RedisUtil.getJedis().set("newname", "中文测试");
        System.out.println(RedisUtil.getJedis().get("newname"));
    }
```

#### SpringBoot集成Redis

Maven依赖

```xml
<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.3.RELEASE</version>
	</parent>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-redis</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-redis</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
	</dependencies>
```

新增配置文件

```properties
########################################################
###Redis (RedisConfiguration)
########################################################
spring.redis.database=0
spring.redis.host=127.0.0.1
spring.redis.port=6379
spring.redis.password=123456
spring.redis.pool.max-idle=8
spring.redis.pool.min-idle=0
spring.redis.pool.max-active=8
spring.redis.pool.max-wait=-1
spring.redis.timeout=5000
```

##### Java代码

```java
@Service
public class RedisService {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	public void setObject(String key, Object value) {
		this.setObject(key, value, null);
	}

	public void setObject(String key, Object value, Long time) {
		if (StringUtils.isEmpty(key) || value == null) {
			return;
		}
		if (value instanceof String) {
			// 存放string类型
			String stringValue = (String) value;
			if (time == null) {
				stringRedisTemplate.opsForValue().set(key, stringValue);
			} else {
				stringRedisTemplate.opsForValue().set(key, stringValue, time, TimeUnit.SECONDS);
			}

			return;
		}
		if (value instanceof List) {
			// 存放list類型
			List<String> listValue = (List<String>) value;
			for (String string : listValue) {
				stringRedisTemplate.opsForList().leftPush(key, string);
			}

		}

	}

	public void delKey(String key) {
		stringRedisTemplate.delete(key);
	}

	public String getString(String key) {
		return stringRedisTemplate.opsForValue().get(key);
	}

} 
```

