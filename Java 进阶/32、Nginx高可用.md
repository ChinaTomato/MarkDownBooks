### 32、Nginx高可用

### linux操作nginx

Nginx是一款轻量级的网页服务器、反向代理服务器。相较于Apache、lighttpd具有占有内存少，稳定性高等优势。它最常的用途是提供反向代理服务。

#### linux安装

在Centos下，yum源不提供nginx的安装，可以通过切换yum源的方法获取安装。也可以通过直接下载安装包的方法，以下命令均需root权限执行：
首先安装必要的库（nginx 中gzip模块需要 zlib 库，rewrite模块需要 pcre 库，ssl 功能需要openssl库）。选定/usr/local为安装目录，以下具体版本号根据实际改变。

##### 安装PCRE库

```
$ cd /usr/local/
$ wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.36.tar.gz
$ tar -zxvf pcre-8.36.tar.gz
$ cd pcre-8.36
$ ./configure
$ make
$ make install
```

./configure报错
configure: error: You need a C++ compiler for C++ support.
解决办法：yum install -y gcc gcc-c++

##### 安装zlib库

```
$ cd /usr/local/ 
$ wget http://zlib.net/zlib-1.2.8.tar.gz
$ tar -zxvf zlib-1.2.8.tar.gz
$ cd zlib-1.2.8
$ ./configure
$ make
$ make install
```

##### 安装ssl

```
$ cd /usr/local/
$ wget http://www.openssl.org/source/openssl-1.0.1j.tar.gz
$ tar -zxvf openssl-1.0.1j.tar.gz
$ ./config
$ make
$ make install
```

##### 安装nginx

```
$ cd /usr/local/
$ wget http://nginx.org/download/nginx-1.8.0.tar.gz
$ tar -zxvf nginx-1.8.0.tar.gz
$ cd nginx-1.8.0  
$ ./configure --prefix=/usr/local/nginx 
$ make
$ make install
```

安装常见错误:
Nginx启动提示找不到libpcre.so.1解决方法
如果是32位系统
[root@lee ~]#  ln -s /usr/local/lib/libpcre.so.1 /lib
如果是64位系统
[root@lee ~]#  ln -s /usr/local/lib/libpcre.so.1 /lib64
然后在启动nginx就OK了
[root@lee ~]# /usr/local/webserver/nginx/sbin/nginx

#### 启动linux

```
$ /usr/local/nginx/sbin/nginx
```

检查是否启动成功：打开浏览器访问此机器的 IP，如果浏览器出现 Welcome to nginx! 则表示 Nginx 已经安装并运行成功。

##### 常用命令

```
重启：
$ /usr/local/nginx/sbin/nginx 启动命令
重启：
$ /usr/local/nginx/sbin/nginx –s reload
停止：
$ /usr/local/nginx/sbin/nginx –s stop
测试配置文件是否正常：
$ /usr/local/nginx/sbin/nginx –t 
强制关闭：
$ pkill nginx
```

#### 启动Nginx+Keepalived高可用

##### 什么是Keepalived

Keepalived是一个免费开源的，用C编写的类似于layer3, 4 & 7交换机制软件，具备我们平时说的第3层、第4层和第7层交换机的功能。主要提供loadbalancing（负载均衡）和 high-availability（高可用）功能，负载均衡实现需要依赖Linux的虚拟服务内核模块（ipvs），而高可用是通过VRRP协议实现多台机器之间的故障转移服务。

![](E:\MarkDownBooks\Java进阶\images\Keepalived功能体系结构.gif)

上图是Keepalived的功能体系结构，大致分两层：用户空间（user space）和内核空间（kernel space）。 
内核空间：主要包括IPVS（IP虚拟服务器，用于实现网络服务的负载均衡）和NETLINK（提供高级路由及其他相关的网络功能）两个部份。 
用户空间：
•	WatchDog：负载监控checkers和VRRP进程的状况
•	VRRP Stack：负载负载均衡器之间的失败切换FailOver，如果只用一个负载均稀器，则VRRP不是必须的。
•	Checkers：负责真实服务器的健康检查healthchecking，是keepalived最主要的功能。换言之，可以没有VRRP Stack，但健康检查healthchecking是一定要有的。
•	IPVS wrapper：用户发送设定的规则到内核ipvs代码
•	Netlink Reflector：用来设定vrrp的vip地址等。
Keepalived的所有功能是配置keepalived.conf文件来实现的。

##### 安装Keepalived

下载keepalived地址：http://www.keepalived.org/download.html
解压安装：
tar -zxvf keepalived-1.2.18.tar.gz -C /usr/local/
yum install -y openssl openssl-devel（需要安装一个软件包）
cd keepalived-1.2.18/ && ./configure --prefix=/usr/local/keepalived
make && make install

##### Keepalived安装成Linux系统服务

将keepalived安装成Linux系统服务，因为没有使用keepalived的默认安装路径（默认路径：/usr/local）,安装完成之后，需要做一些修改工作：
首先创建文件夹，将keepalived配置文件进行复制：
mkdir /etc/keepalived
cp /usr/local/keepalived/etc/keepalived/keepalived.conf /etc/keepalived/
然后复制keepalived脚本文件：
cp /usr/local/keepalived/etc/rc.d/init.d/keepalived /etc/init.d/
cp /usr/local/keepalived/etc/sysconfig/keepalived /etc/sysconfig/
ln -s /usr/local/sbin/keepalived /usr/sbin/
ln -s /usr/local/keepalived/sbin/keepalived /sbin/
可以设置开机启动：chkconfig keepalived on，到此我们安装完毕!

##### Keepalived常用命令

service keepalived start

service keepalived stop

##### 配置nginx主备自动重启

```
第三步：对配置文件进行修改：vim /etc/keepalived/keepalived.conf
keepalived.conf配置文件说明：
（一）Master     

! Configuration File for keepalived

global_defs {
   router_id bhz005 ##标识节点的字符串，通常为hostname
}
## keepalived 会定时执行脚本并且对脚本的执行结果进行分析，动态调整vrrp_instance的优先级。这里的权重weight 是与下面的优先级priority有关，如果执行了一次检查脚本成功，则权重会-20，也就是由100 - 20 变成了80，Master 的优先级为80 就低于了Backup的优先级90，那么会进行自动的主备切换。
如果脚本执行结果为0并且weight配置的值大于0，则优先级会相应增加。
如果脚本执行结果不为0 并且weight配置的值小于0，则优先级会相应减少。
vrrp_script chk_nginx {
    script "/etc/keepalived/nginx_check.sh" ##执行脚本位置
    interval 2 ##检测时间间隔
    weight -20 ## 如果条件成立则权重减20（-20）
}
## 定义虚拟路由 VI_1为自定义标识。
vrrp_instance VI_1 {
state MASTER   ## 主节点为MASTER，备份节点为BACKUP
    ## 绑定虚拟IP的网络接口（网卡），与本机IP地址所在的网络接口相同（我这里是eth6）
interface eth6  
virtual_router_id 172  ## 虚拟路由ID号
    mcast_src_ip 192.168.1.172  ## 本机ip地址
    priority 100  ##优先级配置（0-254的值）
    Nopreempt  ## 
    advert_int 1 ## 组播信息发送间隔，俩个节点必须配置一致，默认1s
    authentication {  
        auth_type PASS
        auth_pass bhz ## 真实生产环境下对密码进行匹配
    }

    track_script {
        chk_nginx
    }

    virtual_ipaddress {
        192.168.1.170 ## 虚拟ip(vip)，可以指定多个
    }
}

（二）Backup
! Configuration File for keepalived

global_defs {
   router_id bhz006
}

vrrp_script chk_nginx {
    script "/etc/keepalived/nginx_check.sh"
    interval 2
    weight -20
}

vrrp_instance VI_1 {
    state BACKUP
    interface eth7
    virtual_router_id 173
    mcast_src_ip 192.168.1.173
    priority 90 ##优先级配置
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass bhz
    }

    track_script {
        chk_nginx
    }

    virtual_ipaddress {
        192.168.1.170
    }
}

（三）nginx_check.sh 脚本：
#!/bin/bash
A=`ps -C nginx –no-header |wc -l`
if [ $A -eq 0 ];then
    /usr/local/nginx/sbin/nginx
    sleep 2
    if [ `ps -C nginx --no-header |wc -l` -eq 0 ];then
        killall keepalived
    fi
fi

（四）我们需要把master的keepalived配置文件 copy到master机器（172）的 /etc/keepalived/ 文件夹下，在把backup的keepalived配置文件copy到backup机器（173）的 /etc/keepalived/ 文件夹下，最后把nginx_check.sh脚本分别copy到两台机器的 /etc/keepalived/文件夹下。

（五）nginx_check.sh脚本授权。赋予可执行权限：chmod +x /etc/keepalived/nginx_check.sh
（六）启动2台机器的nginx之后。我们启动两台机器的keepalived
  /usr/local/nginx/sbin/nginx
  service keepalived start
  ps -ef | grep nginx 
  ps -ef | grep keepalived
  可以进行测试，首先看一下俩台机器的ip a 命令下 都会出现一个虚拟ip，我们可以停掉  一个机器的keepalived，然后测试，命令：service keepalived stop。结果发现当前停掉的机器已经不可用，keepalived会自动切换到另一台机器上。
 （七）我们可以测试在nginx出现问题的情况下，实现切换，这个时候我们只需要把nginx的配置文件进行修改，让其变得不可用，然后强杀掉nginx进程即可，发现也会实现自动切换服务器节点。
```

##### 自动重启不了，解决办法

查看脚本是否有运行的权限
如果你是root登陆的话（不是的话，切换到root用户，对*.sh赋可执行的权限） 

chmod 777 *.sh          赋予权限
 or
chmod +x  *.sh
然后运行就OK了，即有权限对文件进行删除等操作。

##### nginx配置负载均衡

创建一个springboot项目  实现负载均衡

### 集群情况下Session共享解决方案

#### 集群情况下session会产生什么问题

因为Session存放在JVM内存中，集群的话多个JVM不会共享

#### Session共享解决方案

##### nginx或者haproxy做的负载均衡

用Nginx 做的负载均衡可以添加ip_hash这个配置，
用haproxy做的负载均衡可以用 balance source这个配置。
从而使同一个ip的请求发到同一台服务器。

##### 利用数据库同步session

##### 利用cookie同步session数据原理图如下

![](E:\MarkDownBooks\Java进阶\images\cookie同步session数据原理.jpg)

缺点:安全性差、http请求都需要带参数增加了带宽消耗

##### 使用Session集群存放Redis

###### 引入maven依赖

```xml
<!--spring boot 与redis应用基本环境配置 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-redis</artifactId>
        </dependency>
        <!--spring session 与redis应用基本环境配置,需要开启redis后才可以使用，不然启动Spring boot会报错 -->
        <dependency>
            <groupId>org.springframework.session</groupId>
            <artifactId>spring-session-data-redis</artifactId>
        </dependency>
```

###### 创建SessionConfig

```java
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

//这个类用配置redis服务器的连接
//maxInactiveIntervalInSeconds为SpringSession的过期时间（单位：秒）
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 1800)
public class SessionConfig {

	// 冒号后的值为没有配置文件时，制动装载的默认值
	@Value("${redis.hostname:localhost}")
	String HostName;
	@Value("${redis.port:6379}")
	int Port;

	@Bean
	public JedisConnectionFactory connectionFactory() {
		JedisConnectionFactory connection = new JedisConnectionFactory();
		connection.setPort(Port);
		connection.setHostName(HostName);
		return connection;
	}
}
```

###### 初始化Session

```java
//初始化Session配置
public class SessionInitializer extends AbstractHttpSessionApplicationInitializer{
    public SessionInitializer() {
        super(SessionConfig.class);
    }
}
```

###### 配置文件

```properties
server.port=8080

spring.redis.database=0
spring.redis.host=192.168.110.185
spring.redis.port=6379
spring.redis.password=123456
spring.redis.pool.max-idle=8
spring.redis.pool.min-idle=0
spring.redis.pool.max-active=8
spring.redis.pool.max-wait=-1
spring.redis.timeout=5000


redis.hostname=192.168.110.185
redis.port=6379
redis.password=123456
```

###### 控制器层代码

```java
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SessionController {

	@Value("${server.port}")
	private String PORT;

	public static void main(String[] args) {
		SpringApplication.run(SessionController.class, args);
	}

	@RequestMapping("/index")
	public String index() {
		return "index:" + PORT;
	}
	
	@RequestMapping("/setSession")
	public String setSession(HttpServletRequest request, String sessionKey, String sessionValue) {
		HttpSession session = request.getSession(true);
		session.setAttribute(sessionKey, sessionValue);
		return "success,port:" + PORT;
	}
	
	@RequestMapping("/getSession")
	public String getSession(HttpServletRequest request, String sessionKey) {
		HttpSession session =null;
		try {
		 session = request.getSession(false);
		} catch (Exception e) {
		  e.printStackTrace();
		}
		String value=null;
		if(session!=null){
			value = (String) session.getAttribute(sessionKey);
		}
		return "sessionValue:" + value + ",port:" + PORT;
	}

}
```

### 高并发解决方案

业务数据库  -》 数据水平分割(分区分表分库)、读写分离
业务应用 -》 逻辑代码优化(算法优化)、公共数据缓存
应用服务器 -》 反向静态代理、配置优化、负载均衡(apache分发，多tomcat实例)
系统环境 -》 JVM调优
页面优化 -》 减少页面连接数、页面尺寸瘦身
1、动态资源和静态资源分离；
2、CDN；
3、负载均衡；
4、分布式缓存；
5、数据库读写分离或数据切分（垂直或水平）；
6、服务分布式部署。