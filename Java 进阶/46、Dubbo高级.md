## 46、Dubbo高级

### 1.Dubbo-admin管理平台搭建

步骤:
将dubbo-admin.zip 解压到webapps目录下
修改dubbo.properties  zk注册中心连接地址连接信息
启动tomcat即可

### 2.Dubbo集群、负载均衡、容错

步骤
修改配置文件:
provider.xml  端口号 	<dubbo:protocol name="dubbo" port="29015" />
启动两个服务。

### 3.Dubbox

#### 3.1.什么是Dubbox

#### 3.2.Dubbox环境搭建

##### 3.2.1.生产者环境搭建

###### 定义UserService

```java
public interface UserService {
	public String getUser(Integer id);
}
```

###### UserServiceImpl

```java
@Path("users")
public class UserServiceImpl implements UserService {
	@GET
	@Path("{id : \\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUser(@PathParam("id")Integer id) {
		if (id == 1) {
			return "yushengjun";
		}
		if (id == 2) {
			return "zhangsan";
		}
		return "not user info";
	}
}
```

###### dubbo-provider.xml

```properties
<?xml version="1.0" encoding="utf-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
	xsi:schemaLocation="http://www.springframework.org/schema/beans           http://www.springframework.org/schema/beans/spring-beans.xsd           http://code.alibabatech.com/schema/dubbo           http://code.alibabatech.com/schema/dubbo/dubbo.xsd ">

	<!-- 提供方应用信息，用于计算依赖关系 -->
	<dubbo:application name="dubbox-provider" />
	<!-- 使用zookeeper注册中心暴露服务地址 -->
	<dubbo:registry address="zookeeper://127.0.0.1:2181" />
	<!-- 用rest协议在8080端口暴露服务 -->
	<dubbo:protocol name="rest" port="8081" />
	<!-- 声明需要暴露的服务接口 -->
	<dubbo:service interface="com.itmayiedu.service.UserService"
		ref="userService" />

	<!-- 和本地bean一样实现服务 -->
	<bean id="userService" class="com.itmayiedu.service.impl.UserServiceImpl" />
</beans>
```

###### 启动dubbo服务

```java
public class Provider {

	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("dubbo-provider.xml");
		applicationContext.start();
		System.out.println("生产者已经启动...");
		System.in.read();
		
	}

}
```

##### 3.2.2.消费者环境搭建

###### UserService

```java
@Path("users")
public interface UserService {
	@GET
	@Path("{id : \\d+}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUser(@PathParam("id")Integer id);
}
```

###### dubbo.consumer.xml

```properties
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
	xmlns:util="http://www.springframework.org/schema/util"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
	http://code.alibabatech.com/schema/dubbo http://code.alibabatech.com/schema/dubbo/dubbo.xsd
	http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-3.0.xsd">
    <!-- 提供方应用信息，用于计算依赖关系 -->
	<dubbo:application name="consumer"  />
	<dubbo:registry address="zookeeper://127.0.0.1:2181" />
	<!-- 生成远程服务代理，可以像使用本地bean -->
	<dubbo:reference interface="com.itmayiedu.service.UserService"
		id="userService" check="false" />
</beans>
```

###### 调用生产者服务

```java
public class Consumer {

	 public static void main(String[] args) {
		 ClassPathXmlApplicationContext applicationContext= new ClassPathXmlApplicationContext("dubbo-consumer.xml");
		 applicationContext.start();
		 System.out.println("###消费者启动####");
		 UserService userService=(UserService) applicationContext.getBean("userService");
		 System.out.println("消费者调用生产者服务开始");
		 String user = userService.getUser(1);
		 System.out.println("消费者调用生产者服务结束 user:"+user);
		 
	 }
	
}
```

### 4.常见Dubbo面试题

#### 4.1.什么是Dubbo

Dubbo是一个RPC远程调用框架， 分布式服务治理框架
什么是Dubbo服务治理？
服务与服务之间会有很多个Url、依赖关系、负载均衡、容错、自动注册服务。

#### 4.2.Dubbo有哪些协议

默认用的dubbo协议、Http、RMI、Hessian

#### 4.3.Dubbo整个架构流程

分为四大模块
生产者、消费者、注册中心、监控中心
生产者：提供服务
消费者: 调用服务
注册中心:注册信息(redis、zk)
监控中心:调用次数、关系依赖等。
首先生产者将服务注册到注册中心（zk），使用zk持久节点进行存储，消费订阅zk节点，一旦有节点变更，zk通过事件通知传递给消费者，消费可以调用生产者服务。
服务与服务之间进行调用，都会在监控中心中，存储一个记录。

#### 4.4.Dubbox与Dubbo区别

Dubox使用http协议+rest风格传入json或者xml格式进行远程调用。
Dubbo使用Dubbo协议。

##### RPC远程调用框架

SpringCloud、dubbo、Dubbox、thint、Hessian…
Rpc其实就是远程调用，服务与服务之间相互进行通讯。
目前主流 用http+json

#### 4.5.SpringCloud与Dubbo区别

##### 相同点

dubbo与springcloud都可以实现RPC远程调用。
dubbo与springcloud都可以使用分布式、微服务场景下。

##### 区别

dubbo有比较强的背景,在国内有一定影响力。
dubbo使用zk或redis作为作为注册中心
springcloud使用eureka作为注册中心
dubbo支持多种协议，默认使用dubbo协议。
Springcloud只能支持http协议。
Springcloud是一套完整的微服务解决方案。
Dubbo目前已经停止更新,SpringCloud更新速度快。

#### 常见错误

在启动dubbo-admin时我们会先启动zookeeper,如果项目跑到zkclient.ZkEventThread - Starting ZkClient event thread.就不走了，那么就是zookeeper没有跑起来，如果你在zookeeper的目录下运行了./zookeeper-3.3.6/bin/zkServer.sh start 还是这样的话，那么请执行/zookeeper-3.3.6/bin/zkServer.sh status命令看看zookeeper是否跑起来了，如果没跑起来很可能是因为你zookeeper下的conf目录没有zoo.cfg

==解决办法：吧conf目录下的zoo_sample.cfg复制一份为zoo.cfg.==