## 18、Linux开发01

### Linux入门

#### 什么是Linux

#### Linux简介

Linux是一种自由和开放源码的操作系统，存在着许多不同的Linux版本，但它们都使用了Linux内核。Linux可安装在各种计算机硬件设备中，比如手机、平板电脑、路由器、台式计算机

![](E:\MarkDownBooks\Java进阶\images\Linux系统.png)

#### Linux介绍

Linux出现于1991年，是由芬兰赫尔辛基大学学生Linus Torvalds和后来加入的众多爱好者共同开发完成

#### Linux特点

多用户，多任务，丰富的网络功能，可靠的系统安全，良好的可移植性，具有标准兼容性，良好的用户界面，出色的速度性能
开源

#### CentOS

主流：目前的Linux操作系统主要应用于生产环境，主流企业级Linux系统仍旧是RedHat或者CentOS
免费：RedHat 和CentOS差别不大，基于Red Hat Linux 提供的可自由使用源代码的企业CentOS是一个级Linux发行版本
更新方便：CentOS独有的yum命令支持在线升级，可以即时更新系统，不像RedHat 那样需要花钱购买支持服务！

##### CentOS6.4

CentOS官网：http://www.centos.org/
CentOS搜狐镜像：http://mirrors.sohu.com/centos/
CentOS网易镜像：http://mirrors.163.com/centos/
CentOS 6.4下载地址：
http://mirrors.sohu.com/centos/6.4/isos/x86_64/CentOS-6.4-x86_64-bin-DVD1.iso
http://mirrors.sohu.com/centos/6.4/isos/x86_64/CentOS-6.4-x86_64-bin-DVD2.iso

#### Linux安装

环境：
1:Windows7 
2:VMware Workstation9
3: CentOS6.4
安装步骤：见CentOS6.4详细安装文档.doc

#### Linux目录

bin  (binaries)存放二进制可执行文件
sbin  (super user binaries)存放二进制可执行文件，只有root才能访问
etc (etcetera)存放系统配置文件
usr  (unix shared resources)用于存放共享的系统资源
home 存放用户文件的根目录
root  超级用户目录
dev (devices)用于存放设备文件
lib  (library)存放跟文件系统中的程序运行所需要的共享库及内核模块
mnt  (mount)系统管理员安装临时文件系统的安装点
boot 存放用于系统引导时使用的各种文件
tmp  (temporary)用于存放各种临时文件
var  (variable)用于存放运行时需要改变数据的文件

#### Linux客户端

#### 常用命令

```
命令格式：命令  -选项  参数
如：ls  -la  /usr
ls：显示文件和目录列表(list)
常用参数：
-l	(long)
-a	(all) 注意隐藏文件、特殊目录.和..   
-t	(time)
```

```
pwd 显示当前工作目录（print working directory）
touch或者> 创建空文件				                    
mkdir 创建目录（make directoriy）
-p 父目录不存在情况下先生成父目录 （parents）            
cp 复制文件或目录（copy）
-r 递归处理，将指定目录下的文件与子目录一并拷贝（recursive）     
mv 移动文件或目录、文件或目录改名（move）
```

```
rm 删除文件（remove）
-r 同时删除该目录下的所有文件（recursive）
-f 强制删除文件或目录（force）
rmdir 删除空目录（remove directoriy）
cat显示文本文件内容 （catenate）
more、less 分页显示文本文件内容
head、tail查看文本中开头或结尾部分的内容
haed  -n  5  a.log 查看a.log文件的前5行
tail  -f  b.log 循环读取（fellow）
```

```
创建隐藏文件  mkdir .文件名称

改名 mv itmayiedu_a/ itmayiedu_c

删除文件 rm -r itmayiedu_a/    ----会提示
强制删除文件 rm -r itmayiedu_a/    ----不提示
echo "this is java" > book   写入并且创建book文件

mkdir itmayiedu  ---创建文件夹
cd itmayiedu     ---进入该目录文件夹
mkdir itmayiedu-a itmayiedu-b itmayiedu-b   ---创建多个目录
mv  itmayiedu-c itmayiedu_newc     ---移动文件夹
cd itmayiedu_newc-------进入到itmayiedu_newc目录中
touch itmayiedu_txt001-----创建一个空间
cp itmayiedu_text002 itmayiedu_newc----拷贝文件
find itmayiedu_newc/ itmayiedu_text002  ----查找文件
echo "this is itmayiedu" >>itmayiedu_text002  ---写入值
wc itmayiedu_text002 ---统计文本的行数、字数、字符数
grep 'itmayiedu' itmayiedu_text002 ---在指定的文本文件查找指定的字符串
ln -s itmayiedu_newc/   ---为某一个文件在另外一个位置建立一个同步的链接.当我们需要在不同的目录，用到相同的文件时，我们不需要在每一个需要的目录下都放一个必须相同的文件，我们只要在某个固定的目录，放上该文件，然后在 其它的目录下用ln命令链接（link）它就可以，不必重复的占用磁盘空间
```

#### Linux 输出重定向>和>>的区别是什么?

> "**>**"是定向输出到文件，如果文件不存在，就创建文件；如果文件存在，就将其清空；一般我们备份清理日志文件的时候，就是这种方法：先备份日志，再用`>`，将日志文件清空（文件大小变成0字节）；
>
> "**>>**"这个是将输出内容追加到目标文件中。如果文件不存在，就创建文件；如果文件存在，则将新的内容追加到那个文件的末尾，该文件中的原有内容不受影响。

