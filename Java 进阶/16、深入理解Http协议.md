## 16、深入理解Http协议

### Http协议入门

#### 什么是Http协议

http协议： 对浏览器客户端 和  服务器端 之间**数据传输的格式规范**

### 查看Http协议的工具

1）使用火狐的firebug插件（右键->firebug->网络）
2）使用谷歌的“审查元素”

#### Http协议内容

```
请求（浏览器-》服务器）
GET /day09/hello HTTP/1.1
Host: localhost:8080
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: zh-cn,en-us;q=0.8,zh;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: keep-alive
```

```
响应（服务器-》浏览器）
HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Content-Length: 24
Date: Fri, 30 Jan 2015 01:54:57 GMT

this is hello servlet!!!
```

### Http请求

```
GET /day09/hello HTTP/1.1               -请求行
Host: localhost:8080                    --请求头（多个key-value对象）
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: zh-cn,en-us;q=0.8,zh;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: keep-alive
                                    --一个空行
name=eric&password=123456             --（可选）实体内容
```

#### 请求行

GET /day09/hello HTTP/1.1

##### Http协议版本

http1.0：当前浏览器客户端与服务器端建立连接之后，只能发送一次请求，一次请求之后连接关闭。
http1.1：当前浏览器客户端与服务器端建立连接之后，可以在一次连接中发送多次请求。（基本都使用1.1）

##### 请求资源

URL:  统一资源定位符。http://localhost:8080/day09/testImg.html。只能定位互联网资源。是URI的子集。
URI： 统一资源标记符。/day09/hello。用于标记任何资源。可以是本地文件系统，局域网的资源（//192.168.14.10/myweb/index.html），可以是互联网。

##### 请求方式

常见的请求方式： GET 、 POST、 HEAD、 TRACE、 PUT、 CONNECT 、DELETE
常用的请求方式： GET  和 POST	

表单提交：
```html
<form action="提交地址" method="GET/POST">	
<form>
```

GET vs POST 区别

1）**GET方式提交** 
​	a）地址栏（URI）会跟上参数数据。以？开头，多个参数之间以&分割。

```
GET /day09/testMethod.html?name=eric&password=123456 HTTP/1.1
Host: localhost:8080
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: zh-cn,en-us;q=0.8,zh;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Referer: http://localhost:8080/day09/testMethod.html
Connection: keep-alive
```

​	b）GET提交参数数据有限制，不超过1KB。
​	c）GET方式不适合提交敏感密码。
​	==d）注意： 浏览器直接访问的请求，默认提交方式是GET方式==

2）**POST方式提交**

​	a）参数不会跟着URI后面。参数而是跟在请求的实体内容中。没有？开头，多个参数之间以&分割。

```
POST /day09/testMethod.html HTTP/1.1
Host: localhost:8080
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: zh-cn,en-us;q=0.8,zh;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Referer: http://localhost:8080/day09/testMethod.html
Connection: keep-alive

name=eric&password=123456
```

​	b）POST提交的参数数据没有限制。
​	c）POST方式提交敏感数据。

#### 请求头

```
Accept: text/html,image/*      -- 浏览器接受的数据类型
Accept-Charset: ISO-8859-1     -- 浏览器接受的编码格式
Accept-Encoding: gzip,compress  --浏览器接受的数据压缩格式
Accept-Language: en-us,zh-       --浏览器接受的语言
Host: www.it315.org:80          --（必须的）当前请求访问的目标地址（主机:端口）
If-Modified-Since: Tue, 11 Jul 2000 18:23:51 GMT  --浏览器最后的缓存时间
Referer: http://www.it315.org/index.jsp      -- 当前请求来自于哪里
User-Agent: Mozilla/4.0 (compatible; MSIE 5.5; Windows NT 5.0)  --浏览器类型
Cookie:name=eric                     -- 浏览器保存的cookie信息
Connection: close/Keep-Alive            -- 浏览器跟服务器连接状态。close: 连接关闭  keep-alive：保存连接。
Date: Tue, 11 Jul 2000 18:23:51 GMT      -- 请求发出的时间
```

#### 实体内容

只有POST提交的参数会放到实体内容中

#### HttpServletRequest对象

HttpServletRequest对象作用是用于获取请求数据。

核心的API：
请求行： 
​	request.getMethod();   请求方式
​	request.getRequetURI()   / request.getRequetURL()   请求资源
​	request.getProtocol()   请求http协议版本

请求头：
​	request.getHeader("名称")   根据请求头获取请求值
​	request.getHeaderNames()    获取所有的请求头名称

实体内容:
​	request.getInputStream()   获取实体内容数据

#### 案例-什么是时间戳

很多网站在发布版本之前，都会在URL请求地址后面加上一个实现戳进行版本更新。

####案例-防止非法链接(referer)

非法链接：直接访问资源
referer： **当前请求来自于哪里**。

Code：

```xml
	<filter>
		<filter-name>ImgFilter</filter-name>
		<filter-class>com.itmayiedu.filter.ImgFilter</filter-class>
	</filter>
	<filter-mapping>
		<filter-name>ImgFilter</filter-name>
		<url-pattern>/static/*</url-pattern>
	</filter-mapping>
```

```java
public class ImgFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {
		System.out.println("初始化...");
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("doFilter....");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		//获取请求头中来源
		String referer = req.getHeader("referer");
		//获取当前请求名称
		String serverName = request.getServerName();
		System.out.println("referer:"+referer+"----serverName:"+serverName+":"+serverName);
	    if(referer==null||(!referer.contains(serverName))){
	    	req.getRequestDispatcher("error.png").forward(req, res);
	    	return ;
	    }
        // 非盗取链接，直接放行请求
		chain.doFilter(req, res);
	}

	public void destroy() {

	}
}
```

#### 传递的请求参数如何获取

GET方式： 参数放在URI后面
POST方式： 参数放在实体内容中

获取GET方式参数：request.getQueryString();
获取POST方式参数：request.getInputStream();

问题：但是以上两种不通用，而且获取到的参数还需要进一步地解析。
所以可以使用统一方便的获取参数的方式：	

核心的API：
**request.getParameter("参数名")**;  根据参数名获取参数值（注意，只能获取一个值的参数）
**request.getParameterValue("参数名“)**；根据参数名获取参数值（可以获取多个值的参数）
**request.getParameterNames()**;   获取所有参数名称列表

### Http响应

#### 响应行

```
HTTP/1.1 200 OK                --响应行
Server: Apache-Coyote/1.1         --响应头（key-vaule）
Content-Length: 24 
Date: Fri, 30 Jan 2015 01:54:57 GMT
                                   --一个空行
this is hello servlet!!!                  --实体内容
```

##### Http协议版本

##### 状态码: 服务器处理请求的结果（状态）

常见的状态：
200 ：  表示请求处理完成并完美返回
302：   表示请求需要进一步细化。
304：   读取本地缓存
404：   表示客户访问的资源找不到。
500：   表示服务器的资源发送错误。（服务器内部错误）

#### 常见响应头

**Location**: http://www.it315.org/index.jsp   -表示重定向的地址，该头和302的状态码一起使用。
Server:apache tomcat                 ---表示服务器的类型
Content-Encoding: gzip                 -- 表示服务器发送给浏览器的数据压缩类型
Content-Length: 80                    --表示服务器发送给浏览器的数据长度
Content-Language: zh-cn               --表示服务器支持的语言
**Content-Type**: text/html; charset=GB2312   --表示服务器发送给浏览器的数据类型及内容编码
Last-Modified: Tue, 11 Jul 2000 18:23:51 GMT  --表示服务器资源的最后修改时间
**Refresh**: 1;url=http://www.it315.org     --表示定时刷新
Content-Disposition: attachment; filename=aaa.zip --表示告诉浏览器以下载方式打开资源（下载文件时用到）
Transfer-Encoding: chunked
Set-Cookie:SS=Q0=5Lb_nQ; path=/search   --表示服务器发送给浏览器的cookie信息（会话管理用到）
Expires: -1                           --表示通知浏览器不进行缓存
Cache-Control: no-cache
Pragma: no-cache
Connection: close/Keep-Alive           --表示服务器和浏览器的连接状态。close：关闭连接 keep-alive:保存连接

#### HttpServletResponse对象

HttpServletResponse对象修改响应信息：
响应行： 
​		response.setStatus()  设置状态码
响应头： 
​		response.setHeader("name","value")  设置响应头
实体内容：
​		response.getWriter().writer();   发送字符实体内容
​		response.getOutputStream().writer()  发送字节实体内容

#### 案例-请求重定向(Location)

```java
resp.setStatus(302);
resp.setHeader("Location", "OtherServlet");
```

![](D:\MarkDownBooks\Java进阶\images\请求重定向(Location).png)

### Https与Http

#### Https与Http区别

1、https 协议需要到 ca 申请证书，一般免费证书较少，因而需要一定费用。
2、http 是超文本传输协议，信息是明文传输，https 则是具有安全性的 ssl 加密传输协议。
3、http 和 https 使用的是完全不同的连接方式，用的端口也不一样，前者是 80，后者是 443。
4、http 的连接很简单，是无状态的；HTTPS 协议是由 SSL+HTTP 协议构建的可进行加密传输、身份认证的网络协议，比 http 协议安全。

#### Https工作原理

我们都知道 HTTPS 能够加密信息，以免敏感信息被第三方获取，所以很多银行网站或电子邮箱等等安全级别较高的服务都会采用 HTTPS 协议。
客户端在使用 HTTPS 方式与 Web 服务器通信时有以下几个步骤，如图所示。
（1）客户使用 https 的 URL 访问 Web 服务器，要求与 Web 服务器建立 SSL 连接。
（2）Web 服务器收到客户端请求后，会将网站的证书信息（证书中包含公钥）传送一份给客户端。
（3）客户端的浏览器与 Web 服务器开始协商 SSL 连接的安全等级，也就是信息加密的等级。
（4）客户端的浏览器根据双方同意的安全等级，建立会话密钥，然后利用网站的公钥将会话密钥加密，并传送给网站。
（5）Web 服务器利用自己的私钥解密出会话密钥。
（6）Web 服务器利用会话密钥加密与客户端之间的通信。

![](D:\MarkDownBooks\Java进阶\images\Https工作原理.gif)

#### Https优缺点

虽然说 HTTPS 有很大的优势，但其相对来说，还是存在不足之处的：
（1）HTTPS 协议握手阶段比较费时，会使页面的加载时间延长近 50%，增加 10% 到 20% 的耗电；
（2）HTTPS 连接缓存不如 HTTP 高效，会增加数据开销和功耗，甚至已有的安全措施也会因此而受到影响；
（3）SSL 证书需要钱，功能越强大的证书费用越高，个人网站、小网站没有必要一般不会用。
（4）SSL 证书通常需要绑定 IP，不能在同一 IP 上绑定多个域名，IPv4 资源不可能支撑这个消耗。
（5）HTTPS 协议的加密范围也比较有限，在黑客攻击、拒绝服务攻击、服务器劫持等方面几乎起不到什么作用。最关键的，SSL 证书的信用链体系并不安全，特别是在某些国家可以控制 CA 根证书的情况下，中间人攻击一样可行。

