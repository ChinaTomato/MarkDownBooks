## 19、Linux开发02

### VIM编辑器

vi / vim是Unix / Linux上最常用的文本编辑器而且功能非常强大。只有命令，没有菜单。

#### VIM工作模式

![](E:\MarkDownBooks\Java进阶\images\VIM工作模式.png)

#### VIM插入命令

| **i** | **在光标前插入**                 |
| ----- | -------------------------------- |
| **I** | **在光标当前行开始插入**         |
| **a** | **在光标后插入**                 |
| **A** | **在光标当前行末尾插入**         |
| **o** | **在光标当前行的下一行插入新行** |
| **O** | **在光标当前行的上一行插入新行** |

#### VIM定位命令

| **:set nu**   | 显示行号             |
| ------------- | -------------------- |
| **:set nonu** | **取消行号**         |
| **gg**        | **到文本的第一行**   |
| **G**         | **到文本的最后一行** |
| **:n**        | **到文本的第n行**    |

Q! 不保存
Wq保存

### Rpm

RPM是RedHat Package Manager（RedHat软件包管理工具）的缩写，这一文件格式名称虽然打上了RedHat的标志，但是其原始设计理念是开放式的，现在包括RedHat、CentOS、SUSE等Linux的分发版本都有采用，可以算是公认的行业标准了。RPM文件在Linux系统中的安装最为简便

#### Rpm常用命令

rpm的常用参数
i：安装应用程序（install）
e：卸载应用程序（erase）
vh：显示安装进度；（verbose   hash） 
U：升级软件包；（update） 
qa: 显示所有已安装软件包（query all）
结合grep命令使用
例子：rpm  -ivh  gcc-c++-4.4.7-3.el6.x86_64.rpm

#### 安装MySQL

1、上传安装包

2、查看是否安装

rpm -qa | grep 'mysql-server-5.1.73-3.el6_5.i686'  ----检查是否安装
rpm -ivh mysql-server-5.1.73-3.el6_5.i686.rpm  ---安装软件
rpm -Uvh openssl-1.0.1e-16.el6_5.14.i686.rpm----升级库
rpm -Uvh mysql-libs-5.1.73-3.el6_5.i686.rpm ---升级软件
rpm -ivh mysql-5.1.73-3.el6_5.i686.rpm  ---安装软件
rpm -ivh perl-DBI-1.609-4.el6.i686.rpm  --安装软件
rpm -ivh perl-DBD-MySQL-4.013-3.el6.i686.rpm  ---安装软件
service mysqld start   ----启动服务
mysqladmin -u root password 'root' ---设置mysql密码
select user,Password  from user;  -- 查询表

3、连接MySQL

mysql -u  root –p  ----连接 mysql 输入密码
show databases; ----查询所有数据库

### Linux网络设置

设置成静态IP
方式1：

![](E:\MarkDownBooks\Java进阶\images\Linux网络设置.png)

方式2：

vi /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE="eth0" 
BOOTPROTO=“static" 
HWADDR="00:0C:29:62:4C:2B" 
IPV6INIT="yes" 
NM_CONTROLLED="yes" 
ONBOOT="yes" 
TYPE="Ethernet" 
UUID="1acc3359-b1fd-4ac8-b044-58b5fe5a16ce“ 
IPADDR="192.168.24.20" 
NETMASK="255.255.255.0" 
GATEWAY="192.168.24.1" 
DNS1="8.8.8.8" 
DNS2="8.8.4.4"

### Linux安装软件

#### java环境

vim /etc/profile  末尾添加jdk环境

```
export JAVA_HOME=/usr/java/jdk1.7.0_55
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$JAVA_HOME/bin:$PATH
```

刷新profile

source /etc/profile
java –version

```
java version "1.7.0_55"
Java(TM) SE Runtime Environment (build 1.7.0_55-b13)
Java HotSpot(TM) Client VM (build 24.55-b03, mixed mode)
```

#### 安装tomcat

#### 解压tomcat

```
tar -zxvf apache-tomcat-7.0.47.tar.gz
```

#### 启动

```
cd bin
./startup.sh
```

#### 设置防火墙端口

```
打开防火墙对端口限制的方法：
/sbin/iptables -I INPUT -p tcp --dport 8080 -j ACCEPT    #开启8011端口 
/etc/rc.d/init.d/iptables save        #保存配置 
/etc/rc.d/init.d/iptables restart      #重启服务 
#查看端口是否已经开放
/etc/init.d/iptables status 
```

#### 修改tomcat端口

修改tomcat端口号为80

以前只知道当tomcat端口号冲突了如何修改tomcat默认的8080端口号
今天遇到个情况，装了个BO，自带个tomcat，这时就需要修改三个地方
修改Tomcat的端口号：
   在默认情况下，tomcat的端口是8080，如果出现8080端口号冲突，用如下方法可以修改Tomcat的端口号：
首先： 在Tomcat的根（安装）目录下，有一个conf文件夹，双击进入conf文件夹，在里面找到Server.xml文件，打开该文件。
其次：在文件中找到如下文本：
<Connector port="8080" protocol="HTTP/1.1" 
​               maxThreads="150" connectionTimeout="20000" 
​               redirectPort="8443" />
也有可能是这样的：
<Connector port="8080" maxThreads="150" minSpareThreads="25" maxSpareThreads="75" enableLookups="false" redirectPort="8443" acceptCount="100" debug="0" connectionTimeout="20000" 
disableUploadTimeout="true" />等等；

最后：将port="8080"改为其它的就可以了。如port="8081"等。
保存server.xml文件，重新启动Tomcat服务器，Tomcat就可以使用8081端口了。
注意，有的时候要使用两个tomcat，那么就需要修改其中的一个的端口号才能使得两个同时工作。
修改了上面的以后，还要修改两处：
（1）将 <Connector port="8009" enableLookups="false" redirectPort="8443" debug="0"
protocol="AJP/1.3" />的8009改为其它的端口。
（2） 继续将<Server port="8005" shutdown="SHUTDOWN" debug="0">的8005改为其它的端口。
经过以上3个修改，应该就可以了。

#### 关闭所有防火墙

开启： service iptables start 
关闭： service iptables stop 

### 克隆虚拟机

![](E:\MarkDownBooks\Java进阶\images\VMware克隆虚拟机.png)

生成新的mac地址

#### 杀死进程

ps -ef |grep tomcat 
ps aux | grep '6379'  --- 查询端口
kill -15 9886 --- 杀死重置
kill -9 9886 --- 强制杀死