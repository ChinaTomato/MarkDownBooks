## Maven项目管理工具

### 什么是Maven

Maven是一个跨平台的项目管理工具，主要用于基于java平台的项目构建，依赖管理。

![](E:\MarkDownBooks\Java进阶\images\maven.png)

如图为项目构建的过程。

解决的项目的问题：
1、如果有好几个项目，这好几个项目中，需要用到很多相同的jar包，能不能只建立一个仓库来解决这个问题？
2、测试方法能不能全部运行呢？
3、怎么样把一个模块的功能放入到仓库中

### Maven的安装与配置

#### Maven的安装

##### JDK限制

jdk必须1.6以上的版本

##### 官网下载maven

从http://maven.apache.org/官网上下载最新版本的maven

##### 设置path环境变量

把下载下来的maven解压缩，然后有一个bin文件夹，这是一个bin的文件夹的目录F:\work\course\maven\maven\bin
把该目录追加到环境变量的path中。

##### 命令检查是否安装成功

![](E:\MarkDownBooks\Java进阶\images\maven命令检查版本.png)

#### 建库

修改setting.xml中的localRepository节点

#### maven目录解析

bin中存放可执行的二进制文件
conf存放settings.xml文件
lib  运行maven所依赖的jar包

#### maven的约定

src/main/java      存放项目的java文件
src/main/resources  存放项目的资源文件，如spring，hibernate的配置文件
src/test/java       存放所有的测试的java文件
src/test/resources   存放测试用的资源文件
target            项目输出位置
pom.xml  文件

### Maven项目

#### pom文件

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<!-- 属于那些分组,一般以公司名称名称开头 -->
	<groupId>com.itmayiedu</groupId>
	<!-- 一般为项目名称 -->
	<artifactId>Hellofriend</artifactId>
	<version>0.0.1</version>
	<dependencies>
		<dependency>
			<!-- 属于那些分组,一般以公司名称名称开头 -->
			<groupId>com.itmayiedu</groupId>
			<!-- 一般为项目名称 -->
			<artifactId>Hello</artifactId>
			<version>0.0.1</version>
		</dependency>
	</dependencies>
</project>
```

groupId：这是项目组的编号，这在组织或项目中通常是独一无二的。
artifactId：这是项目的ID。这通常是项目的名称。 例如，consumer-banking。 除了groupId之外，artifactId还定义了artifact在存储库中的位置。
version：这是项目的版本。与groupId一起使用，artifact在存储库中用于将版本彼此分离。 
project：表示一个工程
modelVersion：为版本号

#### 编译项目(mvn compile)

编译成功后（BUILD SUCCESS）：可以看到编译后的文件全部放入到了target里。

#### 清理项目(mvn clean)

清理成功后（BUILD SUCCESS）：可以看到target的目录没有了

#### 项目测试(mvn test)

说明：
​      target/classes
​           存放编译后的类
​      target/test-classes
​           存放编译后的测试类
​      target/surefire-reports
​           存放测试报告
可以看出，只要进行测试，清理和编译可以自动执行了。

#### 打包项目(mvn package)

说明：
​	target/classes
​             编译后的类的路径
​        target/test-classes
​             编译后的测试类的路径
​        target/surefire-reports
​             测试报告
​        target/maven-archiver
​             执行package的归档
​        Hello-0.0.1-SNAPSHOT.jar
​			 执行完package命令后打成的jar包

#### mvn clean install

1、	打开命令行
2、	把当前路径调节到Hello工程的根目录
3、	执行mvn clean install命令，把Hello整个工程放入到仓库中



如果执行成功，则会在仓库中看到。

#### Maven常用命令

编译项目
mvn compile
打包发布
mvn package
清理（删除target目录下编译内容）
mvn clean
mvn install                   打包后将其安装在本地仓库

### Maven的核心概念

#### 项目对象模型

![](E:\MarkDownBooks\Java进阶\images\maven项目对象模型.png)

说明：
​	maven根据pom.xml文件，把它转化成项目对象模型(POM)，这个时候要解析依赖关系，然后去相对应的maven库中查找到依赖的jar包。
​	在clean，compile，test，package等阶段都有相应的Plug-in来做这些事情。而这些plug-in会产生一些中间产物。

#### 插件的位置

在maven解压后的位置F:\work\course\maven\maven有一个bin文件夹，里面有
一个文件**m2.config**文件
​	set maven.home default ${user.home}/m2，其中该路径指明了仓库的存储位置。

其中settings.xml文件中<localRepository>D:/maven-repository</localRepository>

说明了仓库中的位置。

#### maven坐标

##### maven坐标节点解析

groupId：定义当前maven项目属于哪个项目
artifactId：定义实际项目中的某一个模块
version：定义当前项目的当前版本
packaging：定义当前项目的打包方式

根据这些坐标，在maven库中可以找到唯一的jar包

#### 依赖管理

#### 继承管理

#### 仓库管理

可以根据maven坐标定义每一个jar包在仓库中的存储位置。大致为：groupId/artifactId/version/artifactId-version.packaging

##### 仓库的分类

本地仓库
~/.m2/repository/
​	每一个用户也可以拥有一个本地仓库
远程仓库
中央仓库：Maven默认的远程仓库 http://repo1.maven.org/maven2
私服：是一种特殊的远程仓库，它是架设在局域网内的仓库
镜像：用来替代中央仓库，速度一般比中央仓库快

### 使用Maven建立（聚合）多模块功能

### Maven打包原理

一、Maven中央存储库
当你建立一个 Maven 的项目，Maven 会检查你的 pom.xml 文件，以确定哪些依赖下载。首先，Maven 将从本地资源库获得 Maven 的本地资源库依赖资源，如果没有找到，然后把它会从默认的 Maven 中央存储库  http://search.maven.org/  查找下载。
在Maven中，当你声明的库不存在于本地存储库中，也没有不存在于Maven中心储存库，该过程将停止并将错误消息输出到 Maven 控制台。

二、添加远程仓库
默认情况下，Maven从Maven中央仓库下载所有依赖关系。但是，有些库丢失在中央存储库，只有在Java.net或JBoss的储存库远程仓库中能找到。
现在，Maven的依赖库查询顺序更改为：
在 Maven 本地资源库中搜索，如果没有找到，进入下一步，否则退出。
在 Maven 中央存储库搜索，如果没有找到，进入下一步，否则退出。
在Maven的远程存储库搜索，如果没有找到，提示错误信息，否则退出。
![](E:\MarkDownBooks\Java进阶\images\maven打包原理.png)

### Maven依赖冲突解决

Web工程依赖两个不同的maven项目，依赖同一个artifactId但是版本不同，这时候就会产生mavenjar依赖冲突问题。

#### 排除依赖

```xml
	<dependencies>
		<dependency>
			<groupId>com.itmayiedu</groupId>
			<artifactId>itmayiedu-service</artifactId>
			<version>0.0.1-SNAPSHOT</version>

		</dependency>
		<dependency>
			<groupId>com.itmayiedu</groupId>
			<artifactId>itmayiedu-entity</artifactId>
			<version>0.0.1-SNAPSHOT</version>
			<exclusions>
				<exclusion>
					<artifactId>commons-logging</artifactId>
					<groupId>commons-logging</groupId>
				</exclusion>
			</exclusions>
		</dependency>

	</dependencies>
```

### 常见错误

错误原因①:
 pom.xml报错：web.xml is missing and <failOnMissingWebXml> is set to true
解决办法:
出现这个错误的原因是Maven不支持缺少web.xml的web项目
添加Web模块，对项目右键->Java EE Tools->Generate Deployment Descriptor Stub,这样就在
src\main\webapp下面生成了WEB-INF文件夹和web.xml,问题解决.
错误原因②
'parent.relativePath' and 'parent.relativePath' points at wrong local POM @ line 4, column 10
解决办法
在应用parent工程中加上<relativePath>../itmayiedu-parent/pom.xml</relativePath>

```xml
<parent>
		<groupId>com.itmayiedu</groupId>
		<artifactId>itmayiedu-parent</artifactId>
		<version>0.0.1-SNAPSHOT</version>
		<relativePath>../itmayiedu-parent/pom.xml</relativePath>
	</parent>
```

错误原因③
No compiler is provided in this environment. Perhaps you are running on a JRE rather than a JDK?
解决办法 更换为自己本地的jdk即可。







