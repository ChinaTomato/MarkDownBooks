## Chapter 3 UML类图

### 3.1 UML基础介绍

1. UML—Unified modeling language UML(统一建模语言)，是一种用于软件系统分析和设计的语言工具，它用于帮助软件开发人员进行思考和记录思路的结果

2. UML本身是一套符号的规定，就像数学符号和化学符号一样，这些符号用于描述软件模型中的各个元素和他们之间的关系，比如类、接口、实现、泛化、依赖、组合、聚合等，如图:

   ![](D:\MarkDownBooks\设计模式\images\UML基本介绍-图1.png)

   ![](D:\MarkDownBooks\设计模式\images\UML基本介绍-图2.png)

   

3. 使用UML来建模，常用的工具有Rational Rose ,也可以使用一些插件来建模

### 3.2 UML图

画UML图与写文章差不多，都是把自己的思想描述给别人看，关键在于思路和条理，UML图分类：

1. 用例图(use case)
2. 静态结构图：**类图**、对象图、包图、组件图、部署图
3. 动态行为图：交互图（时序图与协作图）、状态图、活动图

### 3.3 UML类图

1. 用于描述系统中的类(对象)本身的组成和类(对象)之间的各种静态关系
2. 类之间的关系：**依赖、泛化（继承）、实现、关联、聚合与组合**

### 3.4 类图-依赖关系(Dependence)

- 只要是在**类中用到了对方，那么他们之间就存在依赖关系**。如果没有对方，连编绎都通过不了。

~~~java
public class PersonServiceBean {
	private PersonDao personDao;//类
    public void save(Person person){}
	public IDCard getIDCard(Integer personid){}
    public void modify(){
		Department department = new Department();
    }
}
public class PersonDao{}
public class IDCard{}
public class Person{}
public class Department{}
~~~

- 对应的类图：

![](D:\MarkDownBooks\设计模式\images\类图-依赖关系.png)

- 小结
  1. 类中用到了对方
  2. 如果是类的成员属性
  3. 如果是方法的返回类型
  4. 是方法接收的参数类型
  5. 方法中使用到

### 3.5 类图-泛化关系(Generalization)

- **泛化关系实际上就是继承关系，是依赖关系的特例**

  ~~~java
  public abstract class DaoSupport{
      public void save(Object entity){}
  	public void delete(Object id){}
  }
  public class PersonServiceBean extends Daosupport{}
  ~~~

- 对应的类图

  ![](D:\MarkDownBooks\设计模式\images\类图-泛化关系.png)

- 小结

  1. 泛化关系实际上就是继承关系
  2. 如果A类继承了B类，我们就说A和B存在泛化关系

### 3.6 类图-实现关系(Implementation)

- 实现关系实际上就是**A类实现B接口**，是**依赖关系的特例**

  ~~~java
  public interface PersonService {
  	public void delete(Interger id);
  }
  public class PersonServiceBean implements PersonService{
      public void delete(Interger id){}
  }
  ~~~

- 对应的类图

  ![](D:\MarkDownBooks\设计模式\images\类图-实现关系.png)

### 3.7 类图-关联关系(Association)

关联关系实际上就是**类与类之间的联系，是依赖关系的特例**

关联具有**导航性**：即双向关系或单向关系

关系具有多重性：如“1”（表示有且仅有一个），“0...”（表示0个或者多个），“0，1”（表示0个或者一个），“n...m”(表示n到m个都可以),“m...*”（表示至少m个）

**单向一对一关系**

~~~java
public class Person {
    private IDCard card;
}
~~~

**双向一对一关系**

~~~java
public class Person {
    private IDCard card;
}

public class IDCard{
    private Person person
}
~~~

![](D:\MarkDownBooks\设计模式\images\类图-关联关系.png)

### 3.8 类图-聚合关系(Aggregation)

#### 3.8.1 基础介绍

​		聚合关系（Aggregation）表示的是**整体和部分的关系，整体与部分可以分开**。聚合关系是**关联关系的特例**，所以他具有关联的**导航性与多重性**。

​		如：一台电脑由键盘(keyboard)、显示器(monitor)，鼠标等组成；组成电脑的各个配件是可以从电脑上分离出来的，使用带空心菱形的实线来表示：

#### 3.8.2 应用实例

![](D:\MarkDownBooks\设计模式\images\类图-聚合关系.png)

*PS：如果Mouse,Monitor和Computer是不可分离的（Mouse、Monitor与Computer一同创建与销毁），则升级为组合关系*

![](D:\MarkDownBooks\设计模式\images\类图-聚合升级为组合.png)

### 3.9 类图-组合关系(Composition)

#### 3.9.1 基础介绍

组合关系：也是整体与部分的关系，但是**整体与部分不可以分开**。

案例：在程序中我们定义实体：Person与IDCard、Head,那么Head和Person就是组合，IDCard和Person就是聚合。

但是如果在程序中Person实体中定义了对IDCard进行**级联删除**，即删除Person时连同IDCard一起删除，那么IDCard和Person就是组合了.

#### 3.9.2 应用实例

- 案例1：

~~~java
public class Person{
    private IDCard card;
	private Head head = new Head();
}
public class IDCard{}
public class Head{}
~~~

​		对应的类图：

![](D:\MarkDownBooks\设计模式\images\类图-组合关系.png)

- 案例2：

  ~~~java
  public class Mouse {
  }
  public class Moniter {
  }
  
  public class Computer {
  	private Mouse mouse; //鼠标可以和computer分离
  	private Moniter moniter;//显示器可以和Computer分离
  	public void setMouse(Mouse mouse) {
  		this.mouse = mouse;
  	}
  	public void setMoniter(Moniter moniter) {
  		this.moniter = moniter;
  	}
  }
  ~~~

  对应的类图：

  ![](D:\MarkDownBooks\设计模式\images\类图-组合关系-案例2.png)

  