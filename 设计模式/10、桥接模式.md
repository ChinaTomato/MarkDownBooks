## Chapter 10 桥接模式

### 10.1 手机操作问题

现在对不同手机类型的不同品牌实现操作编程(比如:开机、关机、上网，打电话等)，如图:

![](D:\MarkDownBooks\设计模式\images\桥接模式-手机操作需求示例图.png)

### 10.2 传统方案

传统方案类图：

![](D:\MarkDownBooks\设计模式\images\桥接模式-手机操作传统方案类图.png)

### 10.3 传统方案分析

1. 扩展性问题(**类爆炸**)，如果我们再增加手机的样式(旋转式)，就需要增加各个品牌手机的类，同样如果我们增加一个手机品牌，也要在各个手机样式类下增加
2. 违反了单一职责原则，当我们增加手机样式时，要同时增加所有品牌的手机，这样增加了代码维护成本
3. 解决方案-使用**桥接模式**

### 10.4 桥接模式(Bridge)-基本介绍

1. 桥接模式(Bridge模式)是指：将实现与抽象放在两个不同的类层次中，使两个层次可以独立改变
2. 是一种结构型设计模式
3. Bridge模式基于类的最小设计原则，通过使用封装、聚合及继承等行为让不同的类承担不同的职责。它的主要特点是把抽象(Abstraction)与行为实现(Implementation)分离开来，从而可以保持各部分的独立性以及应对他们的功能扩展

### 10.5 桥接模式(Bridge)-原理类图

![](D:\MarkDownBooks\设计模式\images\桥接模式-原理类图.png)

- Client类：桥接模式的调用者
- 抽象类(Abstraction) :维护了Implementor /即它的实现类ConcreteImplementorA..,二者是聚合关系, Abstraction充当桥接类
- RefinedAbstraction：是Abstraction抽象类的子类
- Implementor：行为的具体实现类
- ConcreteImplementorA/B：行为的具体实现类
- 从UML图：这里的抽象类和接口是聚合的关系（调用和被调用关系）

### 10.6 桥接模式方案

- 思路类图

  ![](D:\MarkDownBooks\设计模式\images\桥接模式-思路类图.png)

- 代码示例

  ~~~java
  public class Client {
  	public static void main(String[] args) {
  		//获取折叠式手机 (样式 + 品牌 )
  		Phone phone1 = new FoldedPhone(new XiaoMi());
  		
  		phone1.open();
  		phone1.call();
  		phone1.close();
  		
  		System.out.println("=======================");
  		
  		Phone phone2 = new FoldedPhone(new Vivo());
  		
  		phone2.open();
  		phone2.call();
  		phone2.close();
  		
  		System.out.println("==============");
  		
  		UpRightPhone phone3 = new UpRightPhone(new XiaoMi());
  		
  		phone3.open();
  		phone3.call();
  		phone3.close();
  		
  		System.out.println("==============");
  		
  		UpRightPhone phone4 = new UpRightPhone(new Vivo());
  		
  		phone4.open();
  		phone4.call();
  		phone4.close();
  	}
  }
  ~~~

  ~~~java
  public abstract class Phone {
  	//组合品牌
  	private Brand brand;
  
  	//构造器
  	public Phone(Brand brand) {
  		super();
  		this.brand = brand;
  	}
  	
  	protected void open() {
  		this.brand.open();
  	}
  	protected void close() {
  		brand.close();
  	}
  	protected void call() {
  		brand.call();
  	}
  }
  ~~~

  ~~~java
  //折叠式手机类，继承 抽象类 Phone
  public class FoldedPhone extends Phone {
  
  	//构造器
  	public FoldedPhone(Brand brand) {
  		super(brand);
  	}
  	
  	public void open() {
  		super.open();
  		System.out.println(" 折叠样式手机 ");
  	}
  	
  	public void close() {
  		super.close();
  		System.out.println(" 折叠样式手机 ");
  	}
  	
  	public void call() {
  		super.call();
  		System.out.println(" 折叠样式手机 ");
  	}
  }
  ~~~

  ~~~java
  public class UpRightPhone extends Phone {
  	
  		//构造器
  		public UpRightPhone(Brand brand) {
  			super(brand);
  		}
  		
  		public void open() {
  			super.open();
  			System.out.println(" 直立样式手机 ");
  		}
  		
  		public void close() {
  			super.close();
  			System.out.println(" 直立样式手机 ");
  		}
  		
  		public void call() {
  			super.call();
  			System.out.println(" 直立样式手机 ");
  		}
  }
  ~~~

  ~~~java
  //接口
  public interface Brand {
  	void open();
  	void close();
  	void call();
  }
  ~~~

  ~~~java
  public class Vivo implements Brand {
  	@Override
  	public void open() {
  		// TODO Auto-generated method stub
  		System.out.println(" Vivo手机开机 ");
  	}
  
  	@Override
  	public void close() {
  		// TODO Auto-generated method stub
  		System.out.println(" Vivo手机关机 ");
  	}
  
  	@Override
  	public void call() {
  		// TODO Auto-generated method stub
  		System.out.println(" Vivo手机打电话 ");
  	}
  }
  ~~~

  ~~~java
  public class XiaoMi implements Brand {
  	@Override
  	public void open() {
  		// TODO Auto-generated method stub
  		System.out.println(" 小米手机开机 ");
  	}
  
  	@Override
  	public void close() {
  		// TODO Auto-generated method stub
  		System.out.println(" 小米手机关机 ");
  	}
  
  	@Override
  	public void call() {
  		// TODO Auto-generated method stub
  		System.out.println(" 小米手机打电话 ");
  	}
  }
  ~~~

  

### 10.7 桥接模式在JDBC的源码剖析

1. Jdbc的Driver接口，如果从桥接模式来看，Driver就是一个接口，下面可以有MySQL的Driver，Oracle的Driver，这些就可以当做实现接口类

2. 代码分析

   ![](D:\MarkDownBooks\设计模式\images\桥接模式-在JDBC中源码示例.png)

   源码分析类图：

   ![](D:\MarkDownBooks\设计模式\images\桥接模式-JDBC源码分析类图.png)

### 10.8 桥接模式注意事项与细节

1. 实现了抽象和实现部分的分离，从而极大的提供了系统的灵活性，让抽象部分和实现部分独立开来，这有助于系统进行分层设计，从而产生更好的结构化系统
2. 对于系统的高层部分，只需要知道抽象部分和实现部分的接口就可以了，其它的部分由具体业务来完成
3. **桥接模式替代多层继承方案**，可以**减少子类的个数**，降低系统的管理和维护成本
4. 桥接模式的引入增加了系统的理解和设计难度，由于聚合关联关系建立在抽象层，要求开发者针对抽象进行设计和编程
5. 桥接模式要求**正确识别出系统中两个独立变化的维度(抽象与实现)**，因此其使用范围有一定的局限性，即需要有这样的应用场景

### 10.9 常见应用场景

1. 对于那些不希望使用继承或因为多层次继承导致系统类的个数急剧增加的系统，桥接模式尤为适用

2. 常见的应用场景：

   - JDBC驱动程序

   - 银行转账系统

     转账分类:网上转账，柜台转账，AMT转账

     转账用户类型：普通用户，银卡用户，金卡用户.

   - 消息管理

     消息类型：即时消息，延时消息

     消息分类：手机短信，邮件消息，QQ消息.